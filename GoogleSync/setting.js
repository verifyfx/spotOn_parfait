/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
var colors = require('colors');
//var dbConfig = {
//    connectionLimit: 100,
//    host: "localhost",
//    user: "parfait",
//    password: "lxvmvvo0'g0ibP",
//    database: "parfait_calendar"
//};
var dbConfig = {
    connectionLimit: 100,
    host: '192.168.1.110',
    user: 'root',
    password: '',
    database: 'calendar'
};
var skipDownload = false;
var showLogMessage = true;
var loopInterval = 20 * 1000;

var printError = function printError(text) {
    console.log(colors.red.bold(text));
};
var printNormal = function printNormal(text) {
    if (showLogMessage)
        console.log(colors.black.bgWhite(text));
};
var printSuccess = function printSuccess(text) {
    console.log(colors.green.bgBlue.bold(text));
};

module.exports.printNormal = printNormal;
module.exports.skipDownload = skipDownload;
module.exports.showLogMessage = showLogMessage;
module.exports.loopInterval = loopInterval;
module.exports.printError = printError;
module.exports.printSuccess = printSuccess;
module.exports.dbConfig = dbConfig;

