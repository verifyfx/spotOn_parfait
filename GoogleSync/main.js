/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
var icalJs = require('ical.js');
var mysql = require('mysql');
var colors = require('colors');
var Spinner = require('cli-spinner').Spinner;
var moment = require('moment');
var async = require('async');
var request = require("request");
var setting = require('./setting.js');
var fs = require('fs');
var syncRequest = require('sync-request');
var pool = mysql.createPool({
    connectionLimit: setting.dbConfig.connectionLimit,
    host: setting.dbConfig.host,
    user: setting.dbConfig.user,
    password: setting.dbConfig.password,
    database: setting.dbConfig.database,
    debug: false
});
pool.on('enqueue', function () {
    console.log('Waiting for available connection slot');
});
var oldEventList = [];
var spinner = new Spinner("Running...");
//spinner.setSpinnerString('|/-\\');
spinner.setSpinnerString(' -|+x*');
if(setting.showLogMessage)
    spinner.start();

//pool.end(function (err) {
//  // all connections in the pool have ended 
//  console.log('end pool');
//});
//main code
setting.printNormal("start program");

loopGetData();
setInterval(function () {
    async.series([
        function (callback) {
            if (oldEventList.length > 0) {
                checkDeleteEvent();
            }
            callback(null, 1);
        },
        function (callback) {
            oldEventList = [];
            console.log("");
            loopGetData();
            callback(null, 2);
        }
    ]);
}, setting.loopInterval);

function loopGetData() {
    try {
        pool.query("SELECT uid,lastmodified FROM events", function (err, rows, fields) {
            if (err)
                setting.printError("DB events " + err);
            else {
                //get old event
                for (var i in rows) {
                    var event = new Object();
                    var row = rows[i];
                    event.uid = row.uid;
                    event.lastModify = row.lastmodified;
                    event.exist = false;
                    oldEventList.push(event);
                }//end for
            }
            //get my calendar url from db
            pool.query("SELECT * FROM calendars", function (err, rows, fields) {
                if (err)
                    setting.printError("DB calendars " + err);
                else {
                    for (var i in rows) {
                        var row = rows[i];
                        var url = row.ical;
                        var cid = row.cid;
                        if (!setting.skipDownload) {
                            downloadCalendar(url , cid);
                        }
                        var icsString = fs.readFileSync("./ics/" + cid + ".ics", 'utf8');
                        convertEvent(icsString, cid);
                        setting.printNormal("+++ Convert calendar " + cid + " +++");
//                        getCalendar(url, cid);
                    }//end for
                }
            });
        });
    } catch (ex) {
        setting.printError(ex);
//        setInterval(function () {
//            loopGetData();
//        }, setting.loopInterval);       
    }
}
function downloadCalendar(url, cid) {
    var success = false;
    try {
        var res = syncRequest('GET', url);
        if (res.statusCode === 200) {
            var path = "./ics/" + cid + ".ics";
            fs.writeFileSync(path, res.getBody());
            setting.printNormal("--- Downloaded calendar " + cid + " ---");
            success = true;
        } else {
            setting.printError("error " + res.statusCode + " " + url);
        }
    } catch (ex) {
        setting.printError("exception " + url + " = " + ex);
    }
    return success;
}

function convertEvent(jsonString, cid) {
    var jcalData = ICAL.parse(jsonString);
    var comp = new ICAL.Component(jcalData);

    vevents = comp.getAllSubcomponents("vevent");
    if (vevents.length > 0) {
        for (var i in vevents) {
            var e = vevents[i];
            var event = new ICAL.Event(e);
            //TODO check event end date are expired > 3 months
            var result = moment(event.endDate.toJSDate()).isBefore(moment().subtract(3, 'months'));
//            console.log(event.summary + " isBefore " + result);
            if (result) {
                continue;
            }
            if (event.isRecurring()) {
                var olduid = event.uid;
                var i = 0;
                var iterator = event.iterator();
                for (var next = iterator.next(); next; next = iterator.next()) {
                    var details = event.getOccurrenceDetails(next);
                    event.startDate = details.startDate;
                    event.endDate = details.endDate;
                    event.uid = olduid + "_" + i;
                    i++;
                    if (i > 10) {
                        break;
                    }
                    // details.startDate
                    // details.endDate
                    // details.item
                    // details.recurrenceId
                    checkInsertEvent(event, cid);
                }
            } else {
                checkInsertEvent(event, cid);
            }
        }
    }
}
function checkInsertEvent(event, cid) {
    var date = new Date();
    var lastModifyNow = date.getTime();
    var propLastMidify = event._firstProp("last-modified");
    if (typeof propLastMidify !== "undefined" || propLastMidify !== null) {
        lastModifyNow = propLastMidify.toJSDate().getTime();
    }
    //check event are in oldEvent
    var found = false;
    for (var i in oldEventList) {
        var e = oldEventList[i];
        if (e.uid === event.uid) {
            found = true;
            e.exist = true;
            if (e.lastModify == lastModifyNow) {
                //no update
//                console.log("no update " + event.summary);
                return;
            } else {
                //old data : update it!
                updateEvent(event, cid, lastModifyNow);
            }
            break;
        }
    }
    if (!found) {
        //new data : insert it!
        insertEvent(event, cid, lastModifyNow);
        insertEventProperties(event, cid);
    }
}

function insertEvent(event, cid, lastModifyNow) {
    var startDateMs = event.startDate.toJSDate().getTime();
    var endDateMs = event.endDate.toJSDate().getTime();
    if (event.duration.toString() === 'P1D') {
        //fix bug all day event are appear in 2 day
        endDateMs = endDateMs - 1;
    }
    var data = {uid: event.uid, cid: cid, name: event.summary, startdate: startDateMs, enddate: endDateMs, description: event.description, lastmodified: lastModifyNow};
    //REPLACE or INSERT
    pool.query('REPLACE INTO events SET ?', data, function (err, result) {
        if (err) {
            setting.printError("insertEvent Error " + cid + " : " + event.uid);
        } else
            setting.printSuccess("insertEvent " + cid + " : " + event.uid);
    });
}

function updateEvent(event, cid, lastModifyNow) {
    var startDateMs = event.startDate.toJSDate().getTime();
    var endDateMs = event.endDate.toJSDate().getTime();

    if (event.duration.toString() === 'P1D') {
        //fix bug all day event are appear in 2 day
        endDateMs = endDateMs - 1;
    }
    var data = {uid: event.uid, cid: cid, name: event.summary, startdate: startDateMs, enddate: endDateMs, description: event.description, lastmodified: lastModifyNow};
    pool.query('REPLACE INTO events SET ?', data, function (err, result) {
        if (err)
            setting.printError("updateEvent Error " + cid + " : " + event.uid);
        else
            setting.printSuccess("updateEvent " + cid + " : " + event.uid);
    });
}

function insertEventProperties(event, cid) {
    var data = {uid: event.uid};
    pool.query('INSERT IGNORE  INTO eventproperties SET ?', data, function (err, result) {
        if (err)
            setting.printError("insert EventProperties Error " + cid + " : " + event.summary);
    });
}

function checkDeleteEvent() {
    try {
        for (var i in oldEventList) {
            var e = oldEventList[i];
            if (!e.exist) {
                //delete event
                setting.printSuccess("***Delete event " + e.uid);
                deleteOldEvent(e.uid);
                deleteOldEventProperties(e.uid);
            }
        }
    } catch (ex) {
        setting.printError("checkDeleteEvent Error " + ex);
    }
//    console.log("************ checkDeleteEvent Complete ************");
}
function deleteOldEvent(uid) {
    var sql = "DELETE FROM events WHERE uid = '" + uid.toString() + "'";
    pool.query(sql, function (err, result) {
        if (err)
            setting.printError("deleteOldEvent Error " + uid);
    });
}

function deleteOldEventProperties(uid) {
    var sql = "DELETE FROM eventproperties WHERE uid = '" + uid.toString() + "'";
    pool.query(sql, function (err, result) {
        if (err)
            setting.printError("deleteOldEventProperties Error " + uid);
    });
}
