/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/* global ICAL */
var mysql = require('mysql');
require('ical.js');
var async = require('async');
var request = require("request");
var setting = require('./setting.js');
var pool = mysql.createPool({
    connectionLimit: setting.dbConfig.connectionLimit,
    host: setting.dbConfig.host,
    user: setting.dbConfig.user,
    password: setting.dbConfig.password,
    database: setting.dbConfig.database,
    debug: false
});
pool.on('enqueue', function () {
  console.log('Waiting for available connection slot');
});
//pool.end(function (err) {
//  // all connections in the pool have ended 
//  console.log('end pool');
//});
loopGetData();
setInterval(function () {
    loopGetData();
}, setting.loopInterval);

function loopGetData() {
//get my calendar url from db
    pool.query("SELECT * FROM calendars", function (err, rows, fields) {
        if (err)
            throw err;
        else {
            deleteAllOldEvent();
//            deleteOldEventProperties();
            for (var i in rows) {
                var row = rows[i];
                var url = row.ical;
                var cid = row.cid;
                var hash = row.hash;
                getCalendar(url, cid);
            }//end for
//            var row = rows[1];
//            var url = row.ical;
//            var cid = row.cid;
//            getCalendar(url, cid);
        }
    });
}

function getCalendar(url, cid) {
    request({
        url: url,
        json: true
    }, function (error, response, body) {
        if (error && response.statusCode !== 200) {
            console.log(error);
        } else {
            console.log('---------------------------------------Download ics complete---------------------------------------' + cid);
            
            convertEvent(body, cid);
//            async.series([
//                function (callback) {
//                    //do some functions
//                    deleteOldEvent(cid);
//                    callback(null, 1); //error = null && response = 1
//                },
//                function (callback) {
//                    //do some functions
//                    convertEvent(body, cid);
//                    callback(null, 2);
//                }
//            ]);
        }
    });
}

function convertEvent(jsonString, cid) {
    var jcalData = ICAL.parse(jsonString);
    var comp = new ICAL.Component(jcalData);

    vevents = comp.getAllSubcomponents("vevent");
    if (vevents.length > 0) {
        for (var i in vevents) {
            var e = vevents[i];
            var event = new ICAL.Event(e);
                       
            if (event.isRecurring()) {
                var olduid = event.uid;
                var i = 0;
                var iterator = event.iterator();
                for (var next = iterator.next(); next; next = iterator.next()) {
                    var details = event.getOccurrenceDetails(next);
                    event.startDate = details.startDate;
                    event.endDate = details.endDate;
                    event.uid = olduid + "_" + i;
                    i++;
                    if(i > 10){
                        break;
                    }
                    // details.startDate
                    // details.endDate
                    // details.item
                    // details.recurrenceId
                    insertEvent(event, cid);
                    insertEventProperties(event, cid);
                }
            } else {
                insertEvent(event, cid);
                insertEventProperties(event, cid);
            }
        }
    }
}

function insertEvent(event, cid) {
    var date = new Date();
    var timeNow = date.getTime();
    var propLastMidify = event._firstProp("last-modified");
    if(typeof propLastMidify !== "undefined" || propLastMidify !== null){
        timeNow = convertDateStringToMilliseconds( propLastMidify.toJSDate() );       
    }
    var startDateMs = convertDateStringToMilliseconds(event.startDate.toJSDate());
    var endDateMs = convertDateStringToMilliseconds(event.endDate.toJSDate());
    if (event.duration.toString() === 'P1D') {
        //fix bug all day event are appear in 2 day
        endDateMs = endDateMs - 1;
    }
    var data = {uid: event.uid, cid: cid, name: event.summary, startdate: startDateMs, enddate: endDateMs, description: event.description, lastmodified: timeNow};
    //REPLACE  INTO events SET ?
    pool.query('INSERT INTO events SET ?', data, function (err, result) {
        if (err)
            throw err;
        else
            console.log("insertEvent " + event.summary);
    });
}

function insertEventProperties(event, cid) {
    var data = {uid: event.uid};
    pool.query('INSERT IGNORE  INTO eventproperties SET ?', data, function (err, result) {
        if (err)
            throw err;
    });
}
function convertDateStringToMilliseconds(date) {
    var myDate = new Date(date);
    var ms = myDate.getTime();
    return ms;
}
function deleteOldEvent(cid) {
    var sql = "DELETE FROM events WHERE cid = " + cid.toString();
    console.log(sql);
    pool.query(sql, function (err, result) {
        if (err)
            throw err;
        else
            console.log(result.insertId);
    });
}
function deleteAllOldEvent() {
    var sql = 'DELETE FROM events';
    pool.query(sql, function (err, result) {
        if (err)
            throw err;
    });
}
//function deleteOldEventProperties(cid) {
//    var sql = 'DELETE FROM eventproperties';
//    pool.query(sql, function (err, result) {
//        if (err)
//            throw err;
//    });
//}