/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
var icalJs = require('ical.js');
var mysql = require('mysql');
var Spinner = require('cli-spinner').Spinner;
var moment = require('moment');
var async = require('async');
var request = require("request");
var fs = require('fs');
var syncRequest = require('sync-request');
var setting = require('./setting.js');
var md5File = require('md5-file');

var pool = mysql.createPool({
    connectionLimit: setting.dbConfig.connectionLimit,
    host: setting.dbConfig.host,
    user: setting.dbConfig.user,
    password: setting.dbConfig.password,
    database: setting.dbConfig.database,
    debug: false
});
pool.on('enqueue', function () {
    console.log('Waiting for available connection slot');
});

loopGetData();
setInterval(function () {
    loopGetData();
}, setting.loopInterval);

function loopGetData() {
    pool.query("SELECT * FROM calendars", function (err, rows, fields) {
        if (err)
            setting.printError(" cann't access database " + setting.dbConfig.host + " = " + err);
        else {
            for (var i in rows) {
                var row = rows[i];
                var url = row.ical;
                var cid = row.cid;
                var hash = row.hash;
                var fileHash = "";
                if (!setting.skipDownload) {
                    fileHash = downloadCalendar(url, cid);
                    //file hash changed after download because DTSTAMP in ics are changed
                } else {
                    fileHash = md5File.sync("./ics/" + cid + ".ics");
                }
                if (fileHash.length > 0 && fileHash != hash) {
                    //data change insert it!
                    setting.printSuccess("Update " + cid);
                    var icsString = fs.readFileSync("./ics/" + cid + ".ics", 'utf8');
                    setting.printSuccess('start');
                            convertEvent(icsString, cid);
                            updateHash(cid, fileHash);

//                    async.series([
//                        function (callback) {
//                            deleteOldEvent(cid);
//                            callback(null, 1);
//                        },
//                        function (callback) {
//                            setting.printSuccess('start');
//                            convertEvent(icsString, cid);
//                            updateHash(cid, fileHash);
//                            callback(null, 2);
//                        }
//                    ]);
                    
                } else {
                    setting.printSuccess("No Update " + cid);
                }
            }//end for
        }
    });
}
function downloadCalendar(url, cid) {
    var fileHash = "";

    try {
        var res = syncRequest('GET', url);
        if (res.statusCode === 200) {
            var path = "./ics/" + cid + ".ics";
            fs.writeFileSync(path, res.getBody());
            console.log("\n-------------------Download " + cid + " complete-------------------");
            fileHash = md5File.sync(path);
        } else {
            setting.printError(" http status " + url);
        }
    } catch (ex) {
        setting.printError(" exception " + url + " = " + ex);
    }
    return fileHash;
//    async.series([
//        function (callback) {
//            deleteOldEvent(cid);
//            callback(null, 1);
//        },
//        function (callback) {
//            setting.printSuccess('start');
//            
//            convertEvent(res.getBody(),cid);
//            callback(null, 2);
//        }
//    ]);
}
function convertEvent(icalString, cid) {
    var jcalData = ICAL.parse(icalString);
    var comp = new ICAL.Component(jcalData);

    vevents = comp.getAllSubcomponents("vevent");
    if (vevents.length > 0) {
        for (var i in vevents) {
            var e = vevents[i];
            var event = new ICAL.Event(e);
            //TODO check event end date are expired > 3 months
            var result = moment(event.endDate.toJSDate()).isBefore(moment().subtract(3, 'months'));
            if (result) {
                continue;
            }
            if (event.isRecurring()) {
                var olduid = event.uid;
                var i = 0;
                var iterator = event.iterator();
                for (var next = iterator.next(); next; next = iterator.next()) {
                    var details = event.getOccurrenceDetails(next);
                    event.startDate = details.startDate;
                    event.endDate = details.endDate;
                    event.uid = olduid + "_" + i;
                    i++;
                    if (i > 10) {
                        break;
                    }
                    // details.startDate
                    // details.endDate
                    // details.item
                    // details.recurrenceId
                    checkInsertEvent(event, cid);
                }
            } else {
                checkInsertEvent(event, cid);
            }
        }
        console.log('\n-------------------DconvertEvent complete-------------------' + cid);
    }
}
function checkInsertEvent(event, cid) {
    var date = new Date();
    var lastModifyNow = date.getTime();
    var propLastMidify = event._firstProp("last-modified");
    if (typeof propLastMidify !== "undefined" || propLastMidify !== null) {
        lastModifyNow = propLastMidify.toJSDate().getTime();
    }
    insertEvent(event, cid, lastModifyNow);
    insertEventProperties(event, cid);
}
function insertEvent(event, cid, lastModifyNow) {
    var startDateMs = event.startDate.toJSDate().getTime();
    var endDateMs = event.endDate.toJSDate().getTime();
    if (event.duration.toString() === 'P1D') {
        //fix bug all day event are appear in 2 day
        endDateMs = endDateMs - 1;
    }

    var data = {uid: event.uid, cid: cid, name: event.summary, startdate: startDateMs, enddate: endDateMs, description: event.description, lastmodified: lastModifyNow};
    //REPLACE or INSERT
    pool.query('REPLACE INTO events SET ?', data, function (err, result) {
        if (err) {
            setting.printError("insertEvent Error " + event.summary);
        }
//        else
//            console.log("insertEvent " + event.summary);
    });
}
function insertEventProperties(event, cid) {
    var data = {uid: event.uid};
    pool.query('INSERT IGNORE  INTO eventproperties SET ?', data, function (err, result) {
        if (err)
            setting.printError("insertEventProperties Error " + event.summary);
    });
}
function updateHash(cid, hash) {
    pool.query('UPDATE calendars SET hash = ? WHERE cid = ?', [hash, cid.toString()], function (err, result) {
        if (err)
            setting.printError("insertEventProperties Error " + event.summary);
    });
}
function deleteOldEvent(cid) {
    var sql = "DELETE FROM events WHERE cid = " + cid.toString();
    console.log(sql);
    pool.query(sql, function (err, result) {
        if (err)
            setting.printError(err);
        else
            console.log(result.insertId);
    });
}


