//////////////////////////////////////////////////////////////////////
var settings = require('../settings.js');
var mysql = require('mysql');
var pool  = mysql.createPool({connectionLimit: settings.mysql.connectionLimit, host: settings.mysql.host, user: settings.mysql.user, password: settings.mysql.password, database: settings.mysql.database, debug: settings.mysql.debug});
//////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////
var events = function(req,res) {
	var calid;
	var from  = req.query.from;
	var to    = req.query.to;
	var qry;
	if(!isset(req.query.calid) || req.query.calid=="") {
		//all
		qry='SELECT `events`.`uid`, `events`.`name`, `events`.`startdate`, `events`.`enddate` ' +
			   'FROM `events` '+
			   'WHERE `events`.`startdate` BETWEEN '+from+' AND '+to;
	} else {
		//specific
		calid = req.query.calid;
		qry='SELECT `events`.`uid`, `events`.`name`, `events`.`startdate`, `events`.`enddate` ' +
			   'FROM `events` '+
			   'WHERE (`events`.`startdate` BETWEEN '+from+' AND '+to+') AND `events`.`cid`='+calid;
	}

	pool.query(qry, function(err, rows, fields) {
	  if (err) throw err;
	  var result = [];
	  for(i=0; i<rows.length; i++) {
	  	var result_single = {id:rows[i].uid, title:rows[i].name, url:"edit/"+rows[i].uid, class:"event-success", start:rows[i].startdate, end:rows[i].enddate};
	  	result.push(result_single);
	  }

	  var output = {
	  	success: 1,
	  	result: result
	  }
	  //console.log(JSON.stringify(output));
	  res.json(output);
	});
}
//////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////
module.exports = events;
//////////////////////////////////////////////////////////////////////