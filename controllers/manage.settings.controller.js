//imports/////////////////////////////////////////////////////////////
var async        = require('async');
var mysql        = require('mysql');
var settings     = require('../settings.js');
var loadSettings = require('../tools/loadSettings');
var pool         = mysql.createPool({connectionLimit: settings.mysql.connectionLimit, host: settings.mysql.host, user: settings.mysql.user, password: settings.mysql.password, database: settings.mysql.database, debug: settings.mysql.debug});
//////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////
var get = function(req,res) {
    var cid = req.params.cid;
    var settings;
    async.series([
        function(callback) {
            loadSettings(cid,callback);
    }],
    function(err,result) {
        pool.query("SELECT `calendars`.`description` FROM `calendars` WHERE `calendars`.`cid`="+cid, function(err, rows) {
            res.render('calendarsettings', {calname: rows[0].description, settings: result[0]});
        });
    });
};
//////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////
var post = function(req,res) {
    var cid = req.params.cid;
    var result = req.body;
    result.defaultBackgroundColor = result.defaultBackgroundColor.substring(1);
    result.textShadowsColor = result.textShadowsColor.substring(1);
    result.textColorDefault = result.textColorDefault.substring(1);
    result.eventListBackground = result.eventListBackground.substring(1);
    result.eventListTextColor = result.eventListTextColor.substring(1);
    var query = "UPDATE `calendarsettings` SET `defaultBackgroundImageEmpty`="+pool.escape(result.defaultBackgroundImageEmpty)+",`defaultBackgroundImageOccupied`="+pool.escape(result.defaultBackgroundImageOccupied)+",`defaultBackgroundColor`="+pool.escape(result.defaultBackgroundColor)+",`fadeBackgroundImage`="+pool.escape(result.fadeBackgroundImage)+",`textShadowsOffset`="+pool.escape(result.textShadowsOffset)+",`textShadowsFade`="+pool.escape(result.textShadowsFade)+",`textShadowsColor`="+pool.escape(result.textShadowsColor)+",`textColorDefault`="+pool.escape(result.textColorDefault)+",`occupiedText`="+pool.escape(result.occupiedText)+",`emptyText`="+pool.escape(result.emptyText)+",`slideshowInterval`="+pool.escape(result.slideshowInterval)+",`refreshTimeout`="+pool.escape(result.refreshTimeout)+",`eventListBackground`="+pool.escape(result.eventListBackground)+",`eventListTextColor`="+pool.escape(result.eventListTextColor)+", `magnifyFactorHeader`="+pool.escape(result.magnifyFactorHeader)+",`magnifyFactorMain`="+pool.escape(result.magnifyFactorMain)+", `magnifyFactorTime`="+pool.escape(result.magnifyFactorTime)+", `magnifyFactorDesc`="+pool.escape(result.magnifyFactorDesc)+", `magnifyFactorToday`="+pool.escape(result.magnifyFactorToday)+", `magnifyFactorTodayNoEvt`="+pool.escape(result.magnifyFactorTodayNoEvt)+", `upcomingEventText`="+pool.escape(result.upcomingEventText)+", `noUpcomingEventText`="+pool.escape(result.noUpcomingEventText)+" WHERE `calendarsettings`.`cid`="+pool.escape(cid);
    console.log(query);
    pool.query(query, function(err, result) {
        if(err) res.sendStatus(500);
        res.redirect('/manage/');
    });
};
//////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////
module.exports.get = get;
module.exports.post = post;