//////////////////////////////////////////////////////////////////////
var express      = require('express');
var mysql        = require('mysql');
var settings     = require('../settings.js');
var loadSettings = require('../tools/loadSettings');
var pool         = mysql.createPool({connectionLimit: settings.mysql.connectionLimit, host: settings.mysql.host, user: settings.mysql.user, password: settings.mysql.password, database: settings.mysql.database, debug: settings.mysql.debug});
//////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////
var get = function(req,res) {
	var uid = req.params.uid;
	pool.query("SELECT `events`.`uid`, `events`.`name`, `events`.`startdate`, `events`.`enddate`, `events`.`description`, `events`.`lastmodified`, `eventproperties`.`textColor`,`calendars`.`cid` , `calendars`.`description` AS caldesc FROM `events` INNER JOIN `eventproperties` ON `events`.`uid` = `eventproperties`.`uid` INNER JOIN `calendars` ON `events`.`cid`=`calendars`.`cid` WHERE `events`.`uid`='"+uid+"' LIMIT 1", function(err, rows, fields) {
		res.render('edit', {result: rows[0]});
	});
}
//////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////
var post = function(req,res) {
    var result = JSON.parse(req.body.data);
    var uid = result.uid;
    var col = result.color;
    pool.query("INSERT INTO `eventproperties` (`eventproperties`.`uid`, `textColor`) VALUES ('"+uid+"','"+col+"') ON DUPLICATE KEY UPDATE textColor='"+col+"'", function(err, rows, fields) {
        if(err) throw err;
    });
}
//////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////
module.exports.get = get;
module.exports.post = post;
//////////////////////////////////////////////////////////////////////