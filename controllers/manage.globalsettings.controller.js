//imports/////////////////////////////////////////////////////////////
var async        = require('async');
var mysql        = require('mysql');
var settings     = require('../settings.js');
var loadSettings = require('../tools/loadSettings');
var pool         = mysql.createPool({connectionLimit: settings.mysql.connectionLimit, host: settings.mysql.host, user: settings.mysql.user, password: settings.mysql.password, database: settings.mysql.database, debug: settings.mysql.debug});
//////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////
var get = function(req,res) {
    pool.query("SELECT * FROM `globalsettings` WHERE `globalsettings`.`xid`=1", function(err, rows) {
        res.render('globalsettings', {result: rows[0]});
    });
};
//////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////
var post = function(req,res) {
    var result = req.body;
    result.allevent_textColor = result.allevent_textColor.substring(1);
    result.allevent_backgroundColor = result.allevent_backgroundColor.substring(1);
    var query = "UPDATE `globalsettings` SET `allevent_textColor`="+pool.escape(result.allevent_textColor)+",`allevent_backgroundColor`="+pool.escape(result.allevent_backgroundColor)+",`allevent_headlineText`="+pool.escape(result.allevent_headlineText)+",`allevent_headlineTextNoEvent`="+pool.escape(result.allevent_headlineTextNoEvent)+", `allevent_magnifyFactorHead`="+pool.escape(result.allevent_magnifyFactorHead)+", `allevent_magnifyFactorTableHead`="+pool.escape(result.allevent_magnifyFactorTableHead)+", `allevent_magnifyFactorContent`="+pool.escape(result.allevent_magnifyFactorContent)+",`allevent_refreshTimeout`="+pool.escape(result.allevent_refreshTimeout)+" WHERE `globalsettings`.`xid`=1";
    pool.query(query, function(err, result) {
        if(err) res.sendStatus(500);
        res.redirect('/manage/');
    });
};
//////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////
module.exports.get = get;
module.exports.post = post;