//////////////////////////////////////////////////////////////////////
var async        = require('async');
var moment       = require('moment');
var hash         = require('object-hash');
var mysql        = require('mysql');
var settings     = require('../settings.js');
var loadSettings = require('../tools/loadSettings');
var loadGlobalSettings = require('../tools/loadGlobalSettings');
var pool         = mysql.createPool({connectionLimit: settings.mysql.connectionLimit, host: settings.mysql.host, user: settings.mysql.user, password: settings.mysql.password, database: settings.mysql.database, debug: settings.mysql.debug});
//////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////
var get_display = function(req,res) {
	displayCurrentEvent(1,req,res);
};

var get_displaySvc_current = function(req,res) {
	displayCurrentEvent(0,req,res);
};

var get_displayToday = function(req,res) {
    displayTodayEvent(1,req,res);
};

var get_displaySvc_today = function(req,res) {
    displayTodayEvent(0,req,res);
};

var get_displayAllToday = function(req,res) {
    displayAllTodayEvent(1,req,res);
};

var get_displaySvc_allToday = function(req,res) {
    displayAllTodayEvent(0,req,res);
};

var get_displayAllUpcomingToday = function(req,res) {
    displayAllTodayEvent(3,req,res);
};

var get_displaySvc_allUpcomingToday = function(req,res) {
    displayAllTodayEvent(2,req,res);
};
//////////////////////////////////////////////////////////////////////
function isNumeric(n) {
  return !isNaN(parseFloat(n)) && isFinite(n);
}
//////////////////////////////////////////////////////////////////////
function displayCurrentEvent(mode,req,res) {
    var cid = req.params.cid || 0;
    if(!isNumeric(cid)) cid = 0;
    var haveEvent = false;
    var currentTime = moment();
    var uid = 0;
    var heading;
    var description;
    var start;
    var end;
    var color;
    var query1 = "SELECT `events`.`uid`, `events`.`name`, `events`.`startdate`, `events`.`enddate`, `events`.`description`, `eventproperties`.`textColor` FROM `events` INNER JOIN `eventproperties` ON `events`.`uid` = `eventproperties`.`uid` WHERE `events`.`cid`="+cid+" AND "+currentTime+" BETWEEN `events`.`startdate` AND `events`.`enddate` LIMIT 1";
    var query2 = "SELECT * FROM `images` WHERE `images`.`uid`='1'";
    var images = [];
    var response;
    var responseMisc;
    var settings;
    async.series([
        function(callback0) {
            async.series([
                function(callback) {
                    loadSettings(cid, callback);
                }
            ], function(err, result) {
                if(err) res.send(err);
                settings = result[0];
                callback0(null);
            });
        },
        function(callback0) {
            async.series([
                function(callback) {
                    pool.query(query1, function(err,rows,fields) {
                        if(err) throw err;
                        if(rows.length!=0) { //return some result
                            uid = rows[0].uid;
                            heading = rows[0].name;
                            description = rows[0].description;
                            start = rows[0].startdate;
                            end = rows[0].enddate;
                            color = rows[0].textColor;
                            query2 = "SELECT * FROM `images` WHERE `images`.`uid`='"+uid+"'";
                            haveEvent = true;
                            callback(null, 1);
                        } else {
                            callback(null, 1);
                        }
                    });
                },
                function(callback) {
                    if(haveEvent) {
                        pool.query(query2, function(err,rows,fields) {
                            if(err) throw err;
                            if(rows.length!=0) { //the event contains pictures
                                rows.forEach(function(val, index) {
                                    var tmp = {url: '/../uploads/'+val.imgUUID+'/'+val.imgName};
                                    images.push(tmp);
                                });
                            } else { //the event does not have a picture
                                images.push({url: '/'+settings.defaultBackgroundImageOccupied});
                            }
                            callback(null, 2);
                        });
                    } else { //no event is running, put default image
                        images.push({url: '/'+settings.defaultBackgroundImageEmpty});
                        callback(null, 2);
                    }
                }],function(err,result) {
                    if(haveEvent) {
                        var dtStart  = moment.unix(start/1000);
                        var dtEnd    = moment.unix(end/1000);
                        var strStart = dtStart.format('H:mm');
                        var strEnd   = dtEnd.format('H:mm');
                        response = {
                            event: {
                                status: true,
                                text: settings.occupiedText
                            },
                            eventinf: {
                                cid: cid,
                                heading: heading,
                                description: description,
                                start: strStart,
                                end: strEnd,
                                color: color,
                                images: images
                            }
                        };
                        responseMisc = {
                            hash: hash({response: response, settings: settings})
                        }
                    } else { //no event running
                        response = {
                            event: false,
                            eventinf: {
                                cid: cid,
                                heading: settings.emptyText,
                                images: images
                            }
                        };
                        responseMisc = {
                            hash: hash({response: response, settings: settings})
                        }
                    }

                    if(!response.event) {
                        response.eventinf.heading = settings.emptyText;
                        response.eventinf.color = settings.color;
                    }
                    if(mode==1) {
                        res.render('display', {response: response, responseMisc: responseMisc, settings: settings});
                    } else {
                        res.json({response: response, responseMisc: responseMisc, settings: settings});
                    }
            });
        }
    ]);
}

function displayTodayEvent(mode,req,res) {
    var cid = req.params.cid || 0;
    var currentTime = moment();
    var endOfDay    = moment().endOf('day');
    var xCurrentTime= currentTime.format('x');
    var xEndOfDay   = endOfDay.format('x');
    var query = "SELECT * FROM `events` WHERE `events`.`cid`="+cid+" AND (`events`.`startdate` BETWEEN "+xCurrentTime+" AND "+xEndOfDay+") ORDER BY `events`.`startdate` ASC";
    var events = [];
    var response;
    var responseMisc;
    var haveEvent = false;
    var settings;
    async.series([
        function(callback) {
            loadSettings(cid,callback);
        }
    ], function(err,result) {
        settings = result[0];
        pool.query(query, function(err,rows,fields) {
            rows.forEach(function(val, index){
                haveEvent=true;
                var timeSpan = moment.unix(val.startdate/1000).format('H:mm')+' - '+moment.unix(val.enddate/1000).format('H:mm');
                var name = val.name;
                var tmp = {
                    time: timeSpan,
                    name: name
                };
                events.push(tmp);
            });
            if(haveEvent) {
                response = {
                    upcomingEvent: true,
                    events: events
                };
                responseMisc = {
                    cid: cid,
                    hash: hash({response: response, settings: settings})
                }
            } else {
                response = {
                    upcomingEvent: false
                };
                responseMisc = {
                    cid: cid,
                    hash: hash({response: response, settings: settings})
                }
            }
            if(mode==1) {
                res.render('displaytoday', {response: response, responseMisc: responseMisc, settings: settings});
            } else {
                res.json({response: response, responseMisc: responseMisc, settings: settings});
            }
        });
    });
}

function displayAllTodayEvent(mode,req,res) {
    var currentTime = moment();
    var startOfDay  = moment().startOf('day');
    var endOfDay    = moment().endOf('day');
    var xCurrentTime= currentTime.format('x');
    var xStartOfDay = startOfDay.format('x');
    var xEndOfDay   = endOfDay.format('x');
    var query = "SELECT `events`.`name`, `events`.`description`, `events`.`startdate`, `events`.`enddate`, `calendars`.`description` AS 'room', `calendars`.`floor` FROM `calendars` INNER JOIN `events` ON `events`.`cid` = `calendars`.`cid` WHERE `events`.`startdate` BETWEEN '"+xStartOfDay+"' AND '"+xEndOfDay+"' ORDER BY `events`.`startdate` ASC";
    var query2 = "SELECT `events`.`name`, `events`.`description`, `events`.`startdate`, `events`.`enddate`, `calendars`.`description` AS 'room', `calendars`.`floor` FROM `calendars` INNER JOIN `events` ON `events`.`cid` = `calendars`.`cid` WHERE `events`.`enddate` BETWEEN '"+xCurrentTime+"' AND '"+xEndOfDay+"' ORDER BY `events`.`startdate` ASC";
    var events = [];
    var response;
    var responseMisc;
    var haveEvent = false;
    var settings;
    if(mode==3 || mode==2) {
        //AllUpcomingToday, otherwise, leave it be
        query = query2;
    }
    async.series([
        function(callback) {
            //loadGlobalSettings(callback);
            loadGlobalSettings(callback);
        }
    ], function(err,result) {
        settings = result[0]; //get setting from callbacks
        pool.query(query, function(err,rows,fields) {
            rows.forEach(function(val, index){
                haveEvent=true;
                var st = moment.unix(val.startdate/1000);
                var en = moment.unix(val.enddate/1000);
                var isCurrent = (currentTime.isBetween(st,en))?true:false;
                var timeSpan = st.format('HH:mm')+' - '+en.format('HH:mm');
                var tmp = {
                    time: timeSpan,
                    name: val.name,
                    desc: val.description,
                    room: val.room,
                    curr: isCurrent,
                    floor: val.floor
                };
                events.push(tmp);
            });
            var updateUrl;
            if(mode==2||mode==3) {
                updateUrl = '/../displaySvc/allUpcoming';
            } else {
                updateUrl = '/../displaySvc/all';
            }
            if(haveEvent) {
                response = {
                    upcomingEvent: true,
                    events: events
                };
                responseMisc = {
                    cid: -1,
                    updateUrl: updateUrl,
                    hash: hash({response: response, settings: settings})
                }
            } else {
                response = {
                    upcomingEvent: false
                };
                responseMisc = {
                    cid: -1,
                    updateUrl: updateUrl,
                    hash: hash({response: response, settings: settings})
                }
            }
            if(mode==1 || mode==3) { //display actual page
                res.render('displayall', {response: response, responseMisc: responseMisc, settings: settings});
            } else { //display json
                res.json({response: response, responseMisc: responseMisc, settings: settings});
            }
        });
    });
}
//////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////
module.exports.get_display = get_display;
module.exports.get_displaySvc_current = get_displaySvc_current;
module.exports.get_displayToday = get_displayToday;
module.exports.get_displaySvc_today = get_displaySvc_today;
module.exports.get_displayAllToday = get_displayAllToday;
module.exports.get_displaySvc_allToday = get_displaySvc_allToday;
module.exports.get_displayAllUpcomingToday = get_displayAllUpcomingToday;
module.exports.get_displaySvc_allUpcomingToday = get_displaySvc_allUpcomingToday;
//////////////////////////////////////////////////////////////////////