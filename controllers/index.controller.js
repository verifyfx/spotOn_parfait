//////////////////////////////////////////////////////////////////////
var settings = require('../settings.js');
var mysql = require('mysql');
var pool  = mysql.createPool({connectionLimit: settings.mysql.connectionLimit, host: settings.mysql.host, user: settings.mysql.user, password: settings.mysql.password, database: settings.mysql.database, debug: settings.mysql.debug});
//////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////
var index = function(req,res) {
	var result = [];
	var selected;
	var selected_desc;

	pool.query("SELECT * FROM calendars", function(err, rows, fields) {
	  if (err) throw err;
	  for(i=0; i<rows.length; i++) {
	  	var result_single = {cid: rows[i].cid, description: rows[i].description};
	  	result.push(result_single);
	  }
	if(!isset(req.query.cid) || req.query.cid=="") {
		//unsetted cid
		selected = result[0].cid;
		selected_desc = result[0].description;
	} else {
		//setted cid
		selected = req.query.cid;
		for(ex=0;ex<result.length;ex++) {
			if(result[ex].cid==selected) {
				selected_desc = result[ex].description;
				break;
			}
		}
	}
	res.render('index', {result:result, current_cid: selected, current_desc: selected_desc});
	});	
}
//////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////
module.exports = index;
//////////////////////////////////////////////////////////////////////