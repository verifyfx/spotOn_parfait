//import//////////////////////////////////////////////////////////////
var express      = require('express');
var fs           = require('fs');
var rimraf       = require('rimraf');
var mkdirp       = require('mkdirp');
var multiparty   = require('multiparty');
var mysql        = require('mysql');
var settings     = require('../settings.js');
var loadSettings = require('../tools/loadSettings');
var pool         = mysql.createPool({connectionLimit: settings.mysql.connectionLimit, host: settings.mysql.host, user: settings.mysql.user, password: settings.mysql.password, database: settings.mysql.database, debug: settings.mysql.debug});
//////////////////////////////////////////////////////////////////////

//local vars//////////////////////////////////////////////////////////
var fileInputName = process.env.FILE_INPUT_NAME || "qqfile";
var publicDir = process.env.PUBLIC_DIR;
var nodeModulesDir = process.env.NODE_MODULES_DIR;
var uploadedFilesPath = 'public/uploads/';
var chunkDirName = "chunks";
var maxFileSize = process.env.MAX_FILE_SIZE || 0;
//////////////////////////////////////////////////////////////////////

//main routing////////////////////////////////////////////////////////
var post_main = function(req,res) { //
    var form = new multiparty.Form();
    var evtID = '';

    form.parse(req, function(err, fields, files) {
        var partIndex = fields.qqpartindex;
        //console.log('ok:'+JSON.stringify(fields));
        // text/plain is required to ensure support for IE9 and older
        res.set("Content-Type", "text/plain");

        if (partIndex == null) {
            onSimpleUpload(fields, files[fileInputName][0], res);
        }
        else {
            onChunkedUpload(fields, files[fileInputName][0], res);
        }
    });
};

var delete_main = function(req,res) {
    var uuid = req.params.uuid,
        dirToDelete = uploadedFilesPath + uuid;

    rimraf(dirToDelete, function(error) {
        if (error) {
            console.error("Problem deleting file! " + error);
            res.status(500);
        }
        removeFromDatabase(uuid);
        res.send();
    });
};

var get_session = function(req,res) {
    pool.query("SELECT * FROM `images` WHERE `images`.`uid`='"+req.query.uid+"'", function(err, rows, fields) {
        var result = [];
        for(i=0;i<rows.length;i++) {
            row = {name: rows[i].imgName, uuid: rows[i].imgUUID, thumbnailUrl: '../uploads/'+rows[i].imgUUID+'/'+rows[i].imgName};
            result.push(row);
        }
        res.json(result);
    });
};
//////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////
function onSimpleUpload(fields, file, res) {
    var uuid = fields.qquuid,
        responseData = {
            success: false,
            link: ''
        };

    file.name = fields.qqfilename;
    var evtID = fields.uid;
    if (isValid(file.size)) {
        moveUploadedFile(file, uuid, function() {
                responseData.success = true;
                responseData.link = uuid+'/'+file.name;
                addToDatabase(uuid, file.name, evtID);
                res.send(responseData);
            },
            function() {
                responseData.error = "Problem copying the file!";
                res.send(responseData);
            });
    }
    else {
        failWithTooBigFile(responseData, res);
    }
}

//////////////////////////////////////////////////////////////////////
function onChunkedUpload(fields, file, res) {
    var size = parseInt(fields.qqtotalfilesize),
        uuid = fields.qquuid,
        index = fields.qqpartindex,
        totalParts = parseInt(fields.qqtotalparts),
        responseData = {
            success: false,
            link: ''
        };

    file.name = fields.qqfilename;
    var evtID = fields.uid;
    if (isValid(size)) {
        storeChunk(file, uuid, index, totalParts, function() {
                if (index < totalParts-1) {
                    responseData.success = true;
                    responseData.link = uuid+'/'+file.name;
                    addToDatabase(uuid, file.name, evtID);
                    res.send(responseData);
                }
                else {
                    combineChunks(file, uuid, function() {
                            responseData.success = true;
                            responseData.link = uuid+'/'+file.name;
                            addToDatabase(uuid, file.name, evtID);
                            res.send(responseData);
                        },
                        function() {
                            responseData.error = "Problem conbining the chunks!";
                            res.send(responseData);
                        });
                }
            },
            function(reset) {
                responseData.error = "Problem storing the chunk!";
                res.send(responseData);
            });
    }
    else {
        failWithTooBigFile(responseData, res);
    }
}

//Add selected information to database////////////////////////////////
function addToDatabase(uuid, name, uid) {
    pool.query("INSERT INTO `images` (`imgUUID`, `imgName`, `uid`) VALUES ('"+uuid+"','"+name+"','"+uid+"')", function(err, rows, fields) {
        if(err) throw err;
    });
}

//remove selected information from database///////////////////////////
function removeFromDatabase(uuid) {
    pool.query("DELETE FROM `images` WHERE `images`.`imgUUID` = \'"+uuid+"\'", function(err, rows, fields) {
        if(err) throw err;
    });
}

//////////////////////////////////////////////////////////////////////
function failWithTooBigFile(responseData, res) {
    responseData.error = "Too big!";
    responseData.preventRetry = true;
    res.send(responseData);
}

//////////////////////////////////////////////////////////////////////
function isValid(size) {
    return maxFileSize === 0 || size < maxFileSize;
}

//////////////////////////////////////////////////////////////////////
function moveFile(destinationDir, sourceFile, destinationFile, success, failure) {
    mkdirp(destinationDir, function(error) {
        var sourceStream, destStream;

        if (error) {
            console.error("Problem creating directory " + destinationDir + ": " + error);
            failure();
        }
        else {
            sourceStream = fs.createReadStream(sourceFile);
            destStream = fs.createWriteStream(destinationFile);

            sourceStream
                .on("error", function(error) {
                    console.error("Problem copying file: " + error.stack);
                    destStream.end();
                    failure();
                })
                .on("end", function(){
                    destStream.end();
                    success();
                })
                .pipe(destStream);
        }
    });
}

//////////////////////////////////////////////////////////////////////
function moveUploadedFile(file, uuid, success, failure) {
    var destinationDir = uploadedFilesPath + uuid + "/",
        fileDestination = destinationDir + file.name;

    moveFile(destinationDir, file.path, fileDestination, success, failure);
}

//////////////////////////////////////////////////////////////////////
function storeChunk(file, uuid, index, numChunks, success, failure) {
    var destinationDir = uploadedFilesPath + uuid + "/" + chunkDirName + "/",
        chunkFilename = getChunkFilename(index, numChunks),
        fileDestination = destinationDir + chunkFilename;

    moveFile(destinationDir, file.path, fileDestination, success, failure);
}

//////////////////////////////////////////////////////////////////////
function combineChunks(file, uuid, success, failure) {
    var chunksDir = uploadedFilesPath + uuid + "/" + chunkDirName + "/",
        destinationDir = uploadedFilesPath + uuid + "/",
        fileDestination = destinationDir + file.name;


    fs.readdir(chunksDir, function(err, fileNames) {
        var destFileStream;

        if (err) {
            console.error("Problem listing chunks! " + err);
            failure();
        }
        else {
            fileNames.sort();
            destFileStream = fs.createWriteStream(fileDestination, {flags: "a"});

            appendToStream(destFileStream, chunksDir, fileNames, 0, function() {
                    rimraf(chunksDir, function(rimrafError) {
                        if (rimrafError) {
                            console.log("Problem deleting chunks dir! " + rimrafError);
                        }
                    });
                    success();
                },
                failure);
        }
    });
}

//////////////////////////////////////////////////////////////////////
function appendToStream(destStream, srcDir, srcFilesnames, index, success, failure) {
    if (index < srcFilesnames.length) {
        fs.createReadStream(srcDir + srcFilesnames[index])
            .on("end", function() {
                appendToStream(destStream, srcDir, srcFilesnames, index+1, success, failure);
            })
            .on("error", function(error) {
                console.error("Problem appending chunk! " + error);
                destStream.end();
                failure();
            })
            .pipe(destStream, {end: false});
    }
    else {
        destStream.end();
        success();
    }
}

//////////////////////////////////////////////////////////////////////
function getChunkFilename(index, count) {
    var digits = new String(count).length,
        zeros = new Array(digits + 1).join("0");

    return (zeros + index).slice(-digits);
}

//Export//////////////////////////////////////////////////////////////
module.exports.post_main = post_main;
module.exports.delete_main = delete_main;
module.exports.get_session = get_session;