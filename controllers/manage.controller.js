//////////////////////////////////////////////////////////////////////
var async        = require('async');
var mysql        = require('mysql');
var settings     = require('../settings.js');
var loadSettings = require('../tools/loadSettings');
var pool         = mysql.createPool({connectionLimit: settings.mysql.connectionLimit, host: settings.mysql.host, user: settings.mysql.user, password: settings.mysql.password, database: settings.mysql.database, debug: settings.mysql.debug});
//////////////////////////////////////////////////////////////////////

//LOAD CALENDAR PAGES/////////////////////////////////////////////////
var get_manage = function(req,res) {
    pool.query("SELECT * FROM `calendars`", function(err, rows, fields) {
        res.render('manage', {result: rows});
    });
};
//////////////////////////////////////////////////////////////////////

//ADD CALENDARS///////////////////////////////////////////////////////
var post_manage = function(req,res) {
    var data = JSON.parse(req.body.data);
    var icals = data.icals;
    var desc  = data.desc;
    var floor  = data.floor;
    
    async.series([
        function(callback) {
            pool.query("INSERT INTO `calendars` (`calendars`.`ical`, `calendars`.`description`, `calendars`.`floor`) VALUES ('"+icals+"','"+desc+"','"+floor+"')", function(err, result) {
                if(err) callback(err);
                callback(null,result.insertId);
            });
        }
    ], function(err,result) { //callback handler
        if(err) res.send(err);
        var insertId = result[0];
        pool.query("INSERT INTO `calendarsettings` (`calendarsettings`.`cid`) VALUES ("+insertId+")", function(err, result) {
            if(err) callback(err);
                res.sendStatus(200);
        });
    });
};
//////////////////////////////////////////////////////////////////////

//DELETE CALENDAR/////////////////////////////////////////////////////
var get_manage_delete = function(req,res) {
    var cid = req.params.cid;
    pool.query("DELETE FROM `calendars` WHERE `calendars`.`cid` = "+cid);
    pool.query("DELETE FROM `calendarsettings` WHERE `calendarsettings`.`cid` = "+cid);
    res.redirect('/manage');
};
//////////////////////////////////////////////////////////////////////

//ICAL URL TESTER/////////////////////////////////////////////////////
var post_manage_test = function(req,res) {
    var url = req.body.url;
    var request = require('request'); // include request module
    request(url, function (err, resp, data) {
        var result = 'NOTREACHED';
        if(err) {
            result = 'INVALID_URI';
        } else {
            if(resp.statusCode==200) {
                if(data.substring(0,15)==='BEGIN:VCALENDAR') {
                    //THIS IS VALID VCAL
                    result = 'OK';
                } else {
                    //THIS IS NOT VALID VCAL
                    result = 'NOTVALID_ICALS';
                }
            } else {
                //THIS FILE IS NOT EXIST
                result = 'CANNOTACCESS_FORBIDDEN_NOTEXIST';
            }
        }
        res.send(result);
    });
};
//////////////////////////////////////////////////////////////////////

//EXPORTS/////////////////////////////////////////////////////////////
module.exports.get_manage = get_manage;
module.exports.post_manage = post_manage;
module.exports.get_manage_delete = get_manage_delete;
module.exports.post_manage_test = post_manage_test;
//////////////////////////////////////////////////////////////////////