<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>Manage Calendar Settings</title>

		<!-- Bootstrap CSS -->
		<link rel="stylesheet" href="/../css/bootstrap.min.css">

		<link rel="stylesheet" type="text/css" href="/../css/spectrum.css">
		<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
		<!--[if lt IE 9]>
			<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.2/html5shiv.min.js"></script>
			<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
		<![endif]-->
	</head>
	<body>
		<div class="container-fluid">
			<div class="row">
				<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
					<h1>Edit Calendar</h1><br>
					<a href="/manage/" title="">Back to manage</a>
				</div>
			</div>
			<div class="row">
				<div class="col-xs-12 col-sm-10 col-md-6 col-lg-4">
					<form action="" method="POST" role="form">
						<legend>{{calname}}</legend>
						<div class="form-group">
							<label for="">defaultBackgroundImageEmpty</label> <span class="label label-info">Default picture when the room is not in-use</span>
							<input type="text" class="form-control" id="defaultBackgroundImageEmpty" name="defaultBackgroundImageEmpty" placeholder="Input field" value="{{settings.defaultBackgroundImageEmpty}}">
						</div>
						<div class="form-group">
							<label for="">defaultBackgroundImageOccupied</label> <span class="label label-info">Picture for event that does not have any image</span>
							<input type="text" class="form-control" id="defaultBackgroundImageOccupied" name="defaultBackgroundImageOccupied" placeholder="Input field" value="{{settings.defaultBackgroundImageOccupied}}">
						</div>
						<div class="form-group">
							<label for="">defaultBackgroundColor</label> <span class="label label-info">Background color for main zone (in case of image cannot be accessed)</span><BR>
							<input type='text' name='defaultBackgroundColor' id='defaultBackgroundColor' name="defaultBackgroundColor" value='#{{settings.defaultBackgroundColor}}' />
						</div>
						<div class="form-group">
							<label for="">fadeBackgroundImage</label> <span class="label label-info">Fade slideshow / image / background color (0.0 = original image 0.5 = 50% fade to black 1.0 = black)</span><BR><BR>
							<input type="text" class="form-control" id="fadeBackgroundImage" name="fadeBackgroundImage" placeholder="Input field" value="{{settings.fadeBackgroundImage}}">
						</div>
						<div class="form-group">
							<label for="">textShadowsOffset</label> <span class="label label-info">How many pixel will shadows move</span>
							<input type="text" class="form-control" id="textShadowsOffset" name="textShadowsOffset" placeholder="Input field" value="{{settings.textShadowsOffset}}">
						</div>
						<div class="form-group">
							<label for="">textShadowsFade</label> <span class="label label-info">Lower integer will make shadows crisp while higher integer will result in fuzzy shadows</span>
							<input type="text" class="form-control" id="textShadowsFade" name="textShadowsFade" placeholder="Input field" value="{{settings.textShadowsFade}}">
						</div>
						<div class="form-group">
							<label for="">textShadowsColor</label> <span class="label label-info">Shadows color</span><BR>
							<input type='text' name='textShadowsColor' id='textShadowsColor' name="textShadowsColor" value='#{{settings.textShadowsColor}}' />
						</div>
						<div class="form-group">
							<label for="">textColorDefault</label> <span class="label label-info">Default text color</span><BR><BR>
							<input type='text' name='textColorDefault' id='textColorDefault' name="textColorDefault" value='#{{settings.textColorDefault}}' />
						</div>
						<div class="form-group">
							<label for="">occupiedText</label> <span class="label label-info">Text to show on top of the screen when the room is in use</span>
							<input type="text" class="form-control" id="occupiedText" name="occupiedText" placeholder="Input field" value="{{settings.occupiedText}}">
						</div>
						<div class="form-group">
							<label for="">emptyText</label> <span class="label label-info">Text to show when the room is empty</span>
							<input type="text" class="form-control" id="emptyText" name="emptyText" placeholder="Input field" value="{{settings.emptyText}}">
						</div>
						<div class="form-group">
							<label for="">slideshowInterval</label> <span class="label label-info">How long will picture last</span> <span class="label label-warning">in milliseconds</span>
							<input type="text" class="form-control" id="slideshowInterval" name="slideshowInterval" placeholder="Input field" value="{{settings.slideshowInterval}}">
						</div>
						<div class="form-group">
							<label for="">refreshTimeout</label> <span class="label label-info">Wait interval between check for new content</span> <span class="label label-warning">in milliseconds</span> <span class="label label-danger">Require Zone Refresh</span>
							<input type="text" class="form-control" id="refreshTimeout" name="refreshTimeout" placeholder="Input field" value="{{settings.refreshTimeout}}">
						</div>
						<div class="form-group">
							<label for="">eventListBackground</label> <span class="label label-info">Background color for today event</span><BR>
							<input type='text' name='eventListBackground' id='eventListBackground' name="eventListBackground" value='#{{settings.eventListBackground}}' />
						</div>
						<div class="form-group">
							<label for="">eventListTextColor</label> <span class="label label-info">Background color for today event</span><BR>
							<input type='text' name='eventListTextColor' id='eventListTextColor' name="eventListTextColor" value='#{{settings.eventListTextColor}}' />
						</div>
						<div class="form-group">
							<label for="">magnifyFactorHeader</label> <span class="label label-info">decimals, 0.1 = very big, 1 = normal, 2 = small</span> <span class="label label-warning">Do not change unless you have custom layout</span><BR>
							<input type='number' class="form-control"  name='magnifyFactorHeader' id='magnifyFactorHeader' name="magnifyFactorHeader" value='{{settings.magnifyFactorHeader}}' min="0.01" step="0.01" />
						</div>
						<div class="form-group">
							<label for="">magnifyFactorMain</label> <span class="label label-info">decimals, 0.1 = very big, 1 = normal, 2 = small</span> <span class="label label-warning">Do not change unless you have custom layout</span><BR>
							<input type='number' class="form-control"  name='magnifyFactorMain' id='magnifyFactorMain' name="magnifyFactorMain" value='{{settings.magnifyFactorMain}}' min="0.01" step="0.01" />
						</div>
						<div class="form-group">
							<label for="">magnifyFactorTime</label> <span class="label label-info">decimals, 0.1 = very big, 1 = normal, 2 = small</span> <span class="label label-warning">Do not change unless you have custom layout</span><BR>
							<input type='number' class="form-control"  name='magnifyFactorTime' id='magnifyFactorTime' name="magnifyFactorTime" value='{{settings.magnifyFactorTime}}' min="0.01" step="0.01" />
						</div>
						<div class="form-group">
							<label for="">magnifyFactorDesc</label> <span class="label label-info">decimals, 0.1 = very big, 1 = normal, 2 = small</span> <span class="label label-warning">Do not change unless you have custom layout</span><BR>
							<input type='number' class="form-control"  name='magnifyFactorDesc' id='magnifyFactorDesc' name="magnifyFactorDesc" value='{{settings.magnifyFactorDesc}}' min="0.01" step="0.01" />
						</div>
						<div class="form-group">
							<label for="">magnifyFactorToday</label> <span class="label label-info">decimals, 0.1 = very big, 1 = normal, 2 = small</span> <span class="label label-warning">Do not change unless you have custom layout</span> <span class="label label-danger">Require Zone Refresh</span><BR>
							<input type='number' class="form-control"  name='magnifyFactorToday' id='magnifyFactorToday' name="magnifyFactorToday" value='{{settings.magnifyFactorToday}}' min="0.01" step="0.01" />
						</div>
						<div class="form-group">
							<label for="">magnifyFactorTodayNoEvt</label> <span class="label label-info">decimals, 0.1 = very big, 1 = normal, 2 = small</span> <span class="label label-warning">Do not change unless you have custom layout</span> <span class="label label-danger">Require Zone Refresh</span><BR>
							<input type='number' class="form-control"  name='magnifyFactorTodayNoEvt' id='magnifyFactorTodayNoEvt' name="magnifyFactorTodayNoEvt" value='{{settings.magnifyFactorTodayNoEvt}}' min="0.01" step="0.01" />
						</div>
						<div class="form-group">
							<label for="">upcomingEventText</label> <span class="label label-info">Text to show when there's upcoming event</span> <span class="label label-danger">Require Zone Refresh</span><BR>
							<input type='text' class="form-control"  name='upcomingEventText' id='upcomingEventText' name="upcomingEventText" value='{{settings.upcomingEventText}}' />
						</div>
						<div class="form-group">
							<label for="">noUpcomingEventText</label> <span class="label label-info">Text to show when there's no upcoming event</span> <span class="label label-danger">Require Zone Refresh</span><BR>
							<input type='text' class="form-control"  name='noUpcomingEventText' id='noUpcomingEventText' name="noUpcomingEventText" value='{{settings.noUpcomingEventText}}' />
						</div>
						<button type="submit" class="btn btn-primary">Submit</button>
					</form>
				</div>
			</div>
		</div>

		<!-- jQuery -->
		<script src="/../js/jquery.min.js"></script>
		<!-- Bootstrap JavaScript -->
		<script src="/../js/bootstrap.min.js"></script>

		<script src="/../js/spectrum.js" type="text/javascript" charset="utf-8"></script>
		<script type="text/javascript">

			$(function() {

				$("#defaultBackgroundColor").spectrum({
				    allowEmpty:false,
				    color: "#{{settings.defaultBackgroundColor}}",
				    showInput: true,
				    containerClassName: "full-spectrum",
				    showInitial: true,
				    showPalette: true,
				    showSelectionPalette: true,
				    showAlpha: false,
				    maxPaletteSize: 10,
				    preferredFormat: "hex",
				    showButton: false,
				    hideAfterPaletteSelect: true,

				    palette: [
				        ["rgb(0, 0, 0)", "rgb(67, 67, 67)", "rgb(102, 102, 102)", /*"rgb(153, 153, 153)","rgb(183, 183, 183)",*/
				        "rgb(204, 204, 204)", "rgb(217, 217, 217)", /*"rgb(239, 239, 239)", "rgb(243, 243, 243)",*/ "rgb(255, 255, 255)"],
				        ["rgb(152, 0, 0)", "rgb(255, 0, 0)", "rgb(255, 153, 0)", "rgb(255, 255, 0)", "rgb(0, 255, 0)",
				        "rgb(0, 255, 255)", "rgb(74, 134, 232)", "rgb(0, 0, 255)", "rgb(153, 0, 255)", "rgb(255, 0, 255)"],
				        ["rgb(230, 184, 175)", "rgb(244, 204, 204)", "rgb(252, 229, 205)", "rgb(255, 242, 204)", "rgb(217, 234, 211)",
				        "rgb(208, 224, 227)", "rgb(201, 218, 248)", "rgb(207, 226, 243)", "rgb(217, 210, 233)", "rgb(234, 209, 220)",
				        "rgb(221, 126, 107)", "rgb(234, 153, 153)", "rgb(249, 203, 156)", "rgb(255, 229, 153)", "rgb(182, 215, 168)",
				        "rgb(162, 196, 201)", "rgb(164, 194, 244)", "rgb(159, 197, 232)", "rgb(180, 167, 214)", "rgb(213, 166, 189)",
				        "rgb(204, 65, 37)", "rgb(224, 102, 102)", "rgb(246, 178, 107)", "rgb(255, 217, 102)", "rgb(147, 196, 125)",
				        "rgb(118, 165, 175)", "rgb(109, 158, 235)", "rgb(111, 168, 220)", "rgb(142, 124, 195)", "rgb(194, 123, 160)",
				        "rgb(166, 28, 0)", "rgb(204, 0, 0)", "rgb(230, 145, 56)", "rgb(241, 194, 50)", "rgb(106, 168, 79)",
				        "rgb(69, 129, 142)", "rgb(60, 120, 216)", "rgb(61, 133, 198)", "rgb(103, 78, 167)", "rgb(166, 77, 121)",
				        "rgb(133, 32, 12)", "rgb(153, 0, 0)", "rgb(180, 95, 6)", "rgb(191, 144, 0)", "rgb(56, 118, 29)",
				        "rgb(19, 79, 92)", "rgb(17, 85, 204)", "rgb(11, 83, 148)", "rgb(53, 28, 117)", "rgb(116, 27, 71)",
				        "rgb(91, 15, 0)", "rgb(102, 0, 0)", "rgb(120, 63, 4)", "rgb(127, 96, 0)", "rgb(39, 78, 19)",
				        "rgb(12, 52, 61)", "rgb(28, 69, 135)", "rgb(7, 55, 99)", "rgb(32, 18, 77)", "rgb(76, 17, 48)"]
				    ]
				});
				$("#textShadowsColor").spectrum({
				    allowEmpty:false,
				    color: "#{{settings.textShadowsColor}}",
				    showInput: true,
				    containerClassName: "full-spectrum",
				    showInitial: true,
				    showPalette: true,
				    showSelectionPalette: true,
				    showAlpha: false,
				    maxPaletteSize: 10,
				    preferredFormat: "hex",
				    showButton: false,
				    hideAfterPaletteSelect: true,

				    palette: [
				        ["rgb(0, 0, 0)", "rgb(67, 67, 67)", "rgb(102, 102, 102)", /*"rgb(153, 153, 153)","rgb(183, 183, 183)",*/
				        "rgb(204, 204, 204)", "rgb(217, 217, 217)", /*"rgb(239, 239, 239)", "rgb(243, 243, 243)",*/ "rgb(255, 255, 255)"],
				        ["rgb(152, 0, 0)", "rgb(255, 0, 0)", "rgb(255, 153, 0)", "rgb(255, 255, 0)", "rgb(0, 255, 0)",
				        "rgb(0, 255, 255)", "rgb(74, 134, 232)", "rgb(0, 0, 255)", "rgb(153, 0, 255)", "rgb(255, 0, 255)"],
				        ["rgb(230, 184, 175)", "rgb(244, 204, 204)", "rgb(252, 229, 205)", "rgb(255, 242, 204)", "rgb(217, 234, 211)",
				        "rgb(208, 224, 227)", "rgb(201, 218, 248)", "rgb(207, 226, 243)", "rgb(217, 210, 233)", "rgb(234, 209, 220)",
				        "rgb(221, 126, 107)", "rgb(234, 153, 153)", "rgb(249, 203, 156)", "rgb(255, 229, 153)", "rgb(182, 215, 168)",
				        "rgb(162, 196, 201)", "rgb(164, 194, 244)", "rgb(159, 197, 232)", "rgb(180, 167, 214)", "rgb(213, 166, 189)",
				        "rgb(204, 65, 37)", "rgb(224, 102, 102)", "rgb(246, 178, 107)", "rgb(255, 217, 102)", "rgb(147, 196, 125)",
				        "rgb(118, 165, 175)", "rgb(109, 158, 235)", "rgb(111, 168, 220)", "rgb(142, 124, 195)", "rgb(194, 123, 160)",
				        "rgb(166, 28, 0)", "rgb(204, 0, 0)", "rgb(230, 145, 56)", "rgb(241, 194, 50)", "rgb(106, 168, 79)",
				        "rgb(69, 129, 142)", "rgb(60, 120, 216)", "rgb(61, 133, 198)", "rgb(103, 78, 167)", "rgb(166, 77, 121)",
				        "rgb(133, 32, 12)", "rgb(153, 0, 0)", "rgb(180, 95, 6)", "rgb(191, 144, 0)", "rgb(56, 118, 29)",
				        "rgb(19, 79, 92)", "rgb(17, 85, 204)", "rgb(11, 83, 148)", "rgb(53, 28, 117)", "rgb(116, 27, 71)",
				        "rgb(91, 15, 0)", "rgb(102, 0, 0)", "rgb(120, 63, 4)", "rgb(127, 96, 0)", "rgb(39, 78, 19)",
				        "rgb(12, 52, 61)", "rgb(28, 69, 135)", "rgb(7, 55, 99)", "rgb(32, 18, 77)", "rgb(76, 17, 48)"]
				    ]
				});
				$("#textColorDefault").spectrum({
				    allowEmpty:false,
				    color: "#{{settings.textColorDefault}}",
				    showInput: true,
				    containerClassName: "full-spectrum",
				    showInitial: true,
				    showPalette: true,
				    showSelectionPalette: true,
				    showAlpha: false,
				    maxPaletteSize: 10,
				    preferredFormat: "hex",
				    showButton: false,
				    hideAfterPaletteSelect: true,

				    palette: [
				        ["rgb(0, 0, 0)", "rgb(67, 67, 67)", "rgb(102, 102, 102)", /*"rgb(153, 153, 153)","rgb(183, 183, 183)",*/
				        "rgb(204, 204, 204)", "rgb(217, 217, 217)", /*"rgb(239, 239, 239)", "rgb(243, 243, 243)",*/ "rgb(255, 255, 255)"],
				        ["rgb(152, 0, 0)", "rgb(255, 0, 0)", "rgb(255, 153, 0)", "rgb(255, 255, 0)", "rgb(0, 255, 0)",
				        "rgb(0, 255, 255)", "rgb(74, 134, 232)", "rgb(0, 0, 255)", "rgb(153, 0, 255)", "rgb(255, 0, 255)"],
				        ["rgb(230, 184, 175)", "rgb(244, 204, 204)", "rgb(252, 229, 205)", "rgb(255, 242, 204)", "rgb(217, 234, 211)",
				        "rgb(208, 224, 227)", "rgb(201, 218, 248)", "rgb(207, 226, 243)", "rgb(217, 210, 233)", "rgb(234, 209, 220)",
				        "rgb(221, 126, 107)", "rgb(234, 153, 153)", "rgb(249, 203, 156)", "rgb(255, 229, 153)", "rgb(182, 215, 168)",
				        "rgb(162, 196, 201)", "rgb(164, 194, 244)", "rgb(159, 197, 232)", "rgb(180, 167, 214)", "rgb(213, 166, 189)",
				        "rgb(204, 65, 37)", "rgb(224, 102, 102)", "rgb(246, 178, 107)", "rgb(255, 217, 102)", "rgb(147, 196, 125)",
				        "rgb(118, 165, 175)", "rgb(109, 158, 235)", "rgb(111, 168, 220)", "rgb(142, 124, 195)", "rgb(194, 123, 160)",
				        "rgb(166, 28, 0)", "rgb(204, 0, 0)", "rgb(230, 145, 56)", "rgb(241, 194, 50)", "rgb(106, 168, 79)",
				        "rgb(69, 129, 142)", "rgb(60, 120, 216)", "rgb(61, 133, 198)", "rgb(103, 78, 167)", "rgb(166, 77, 121)",
				        "rgb(133, 32, 12)", "rgb(153, 0, 0)", "rgb(180, 95, 6)", "rgb(191, 144, 0)", "rgb(56, 118, 29)",
				        "rgb(19, 79, 92)", "rgb(17, 85, 204)", "rgb(11, 83, 148)", "rgb(53, 28, 117)", "rgb(116, 27, 71)",
				        "rgb(91, 15, 0)", "rgb(102, 0, 0)", "rgb(120, 63, 4)", "rgb(127, 96, 0)", "rgb(39, 78, 19)",
				        "rgb(12, 52, 61)", "rgb(28, 69, 135)", "rgb(7, 55, 99)", "rgb(32, 18, 77)", "rgb(76, 17, 48)"]
				    ]
				});
				$("#eventListBackground").spectrum({
				    allowEmpty:false,
				    color: "#{{settings.eventListBackground}}",
				    showInput: true,
				    containerClassName: "full-spectrum",
				    showInitial: true,
				    showPalette: true,
				    showSelectionPalette: true,
				    showAlpha: false,
				    maxPaletteSize: 10,
				    preferredFormat: "hex",
				    showButton: false,
				    hideAfterPaletteSelect: true,

				    palette: [
				        ["rgb(0, 0, 0)", "rgb(67, 67, 67)", "rgb(102, 102, 102)", /*"rgb(153, 153, 153)","rgb(183, 183, 183)",*/
				        "rgb(204, 204, 204)", "rgb(217, 217, 217)", /*"rgb(239, 239, 239)", "rgb(243, 243, 243)",*/ "rgb(255, 255, 255)"],
				        ["rgb(152, 0, 0)", "rgb(255, 0, 0)", "rgb(255, 153, 0)", "rgb(255, 255, 0)", "rgb(0, 255, 0)",
				        "rgb(0, 255, 255)", "rgb(74, 134, 232)", "rgb(0, 0, 255)", "rgb(153, 0, 255)", "rgb(255, 0, 255)"],
				        ["rgb(230, 184, 175)", "rgb(244, 204, 204)", "rgb(252, 229, 205)", "rgb(255, 242, 204)", "rgb(217, 234, 211)",
				        "rgb(208, 224, 227)", "rgb(201, 218, 248)", "rgb(207, 226, 243)", "rgb(217, 210, 233)", "rgb(234, 209, 220)",
				        "rgb(221, 126, 107)", "rgb(234, 153, 153)", "rgb(249, 203, 156)", "rgb(255, 229, 153)", "rgb(182, 215, 168)",
				        "rgb(162, 196, 201)", "rgb(164, 194, 244)", "rgb(159, 197, 232)", "rgb(180, 167, 214)", "rgb(213, 166, 189)",
				        "rgb(204, 65, 37)", "rgb(224, 102, 102)", "rgb(246, 178, 107)", "rgb(255, 217, 102)", "rgb(147, 196, 125)",
				        "rgb(118, 165, 175)", "rgb(109, 158, 235)", "rgb(111, 168, 220)", "rgb(142, 124, 195)", "rgb(194, 123, 160)",
				        "rgb(166, 28, 0)", "rgb(204, 0, 0)", "rgb(230, 145, 56)", "rgb(241, 194, 50)", "rgb(106, 168, 79)",
				        "rgb(69, 129, 142)", "rgb(60, 120, 216)", "rgb(61, 133, 198)", "rgb(103, 78, 167)", "rgb(166, 77, 121)",
				        "rgb(133, 32, 12)", "rgb(153, 0, 0)", "rgb(180, 95, 6)", "rgb(191, 144, 0)", "rgb(56, 118, 29)",
				        "rgb(19, 79, 92)", "rgb(17, 85, 204)", "rgb(11, 83, 148)", "rgb(53, 28, 117)", "rgb(116, 27, 71)",
				        "rgb(91, 15, 0)", "rgb(102, 0, 0)", "rgb(120, 63, 4)", "rgb(127, 96, 0)", "rgb(39, 78, 19)",
				        "rgb(12, 52, 61)", "rgb(28, 69, 135)", "rgb(7, 55, 99)", "rgb(32, 18, 77)", "rgb(76, 17, 48)"]
				    ]
				});
				$("#eventListTextColor").spectrum({
				    allowEmpty:false,
				    color: "#{{settings.eventListTextColor}}",
				    showInput: true,
				    containerClassName: "full-spectrum",
				    showInitial: true,
				    showPalette: true,
				    showSelectionPalette: true,
				    showAlpha: false,
				    maxPaletteSize: 10,
				    preferredFormat: "hex",
				    showButton: false,
				    hideAfterPaletteSelect: true,

				    palette: [
				        ["rgb(0, 0, 0)", "rgb(67, 67, 67)", "rgb(102, 102, 102)", /*"rgb(153, 153, 153)","rgb(183, 183, 183)",*/
				        "rgb(204, 204, 204)", "rgb(217, 217, 217)", /*"rgb(239, 239, 239)", "rgb(243, 243, 243)",*/ "rgb(255, 255, 255)"],
				        ["rgb(152, 0, 0)", "rgb(255, 0, 0)", "rgb(255, 153, 0)", "rgb(255, 255, 0)", "rgb(0, 255, 0)",
				        "rgb(0, 255, 255)", "rgb(74, 134, 232)", "rgb(0, 0, 255)", "rgb(153, 0, 255)", "rgb(255, 0, 255)"],
				        ["rgb(230, 184, 175)", "rgb(244, 204, 204)", "rgb(252, 229, 205)", "rgb(255, 242, 204)", "rgb(217, 234, 211)",
				        "rgb(208, 224, 227)", "rgb(201, 218, 248)", "rgb(207, 226, 243)", "rgb(217, 210, 233)", "rgb(234, 209, 220)",
				        "rgb(221, 126, 107)", "rgb(234, 153, 153)", "rgb(249, 203, 156)", "rgb(255, 229, 153)", "rgb(182, 215, 168)",
				        "rgb(162, 196, 201)", "rgb(164, 194, 244)", "rgb(159, 197, 232)", "rgb(180, 167, 214)", "rgb(213, 166, 189)",
				        "rgb(204, 65, 37)", "rgb(224, 102, 102)", "rgb(246, 178, 107)", "rgb(255, 217, 102)", "rgb(147, 196, 125)",
				        "rgb(118, 165, 175)", "rgb(109, 158, 235)", "rgb(111, 168, 220)", "rgb(142, 124, 195)", "rgb(194, 123, 160)",
				        "rgb(166, 28, 0)", "rgb(204, 0, 0)", "rgb(230, 145, 56)", "rgb(241, 194, 50)", "rgb(106, 168, 79)",
				        "rgb(69, 129, 142)", "rgb(60, 120, 216)", "rgb(61, 133, 198)", "rgb(103, 78, 167)", "rgb(166, 77, 121)",
				        "rgb(133, 32, 12)", "rgb(153, 0, 0)", "rgb(180, 95, 6)", "rgb(191, 144, 0)", "rgb(56, 118, 29)",
				        "rgb(19, 79, 92)", "rgb(17, 85, 204)", "rgb(11, 83, 148)", "rgb(53, 28, 117)", "rgb(116, 27, 71)",
				        "rgb(91, 15, 0)", "rgb(102, 0, 0)", "rgb(120, 63, 4)", "rgb(127, 96, 0)", "rgb(39, 78, 19)",
				        "rgb(12, 52, 61)", "rgb(28, 69, 135)", "rgb(7, 55, 99)", "rgb(32, 18, 77)", "rgb(76, 17, 48)"]
				    ]
				});

			});
		</script>
	</body>
</html>