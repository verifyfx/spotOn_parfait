var express      = require('express');
var settings     = require('../settings.js');
var mysql = require('mysql');
var pool  = mysql.createPool({connectionLimit: settings.mysql.connectionLimit, host: settings.mysql.host, user: settings.mysql.user, password: settings.mysql.password, database: settings.mysql.database, debug: settings.mysql.debug});

var loadGlobalSettings = function(callback) {
    var settings = {
        allevent_textColor : '222041',
        allevent_backgroundColor : 'DBB072',
        allevent_headlineText : 'Today\'s Events',
        allevent_headlineTextNoEvent : 'No Upcoming Event',
        allevent_magnifyFactorHead : '1.5',
        allevent_magnifyFactorTableHead : '3',
        allevent_magnifyFactorContent : '3.3',
        allevent_refreshTimeout : '30000'
    };
    pool.query("SELECT * FROM `globalsettings` WHERE `xid`=1", function(err,rows,fields) {
        if(err) return callback(err);
        if(rows.length!=0) {
            settings = {
                allevent_textColor : rows[0].allevent_textColor,
                allevent_backgroundColor : rows[0].allevent_backgroundColor,
                allevent_headlineText : rows[0].allevent_headlineText,
                allevent_headlineTextNoEvent : rows[0].allevent_headlineTextNoEvent,
                allevent_magnifyFactorHead : rows[0].allevent_magnifyFactorHead,
                allevent_magnifyFactorTableHead : rows[0].allevent_magnifyFactorTableHead,
                allevent_magnifyFactorContent : rows[0].allevent_magnifyFactorContent,
                allevent_refreshTimeout : rows[0].allevent_refreshTimeout
            };
        }
        callback(null, settings);
    });
}

module.exports = loadGlobalSettings;