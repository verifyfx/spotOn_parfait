var express      = require('express');
var settings     = require('../settings.js');
var mysql = require('mysql');
var pool  = mysql.createPool({connectionLimit: settings.mysql.connectionLimit, host: settings.mysql.host, user: settings.mysql.user, password: settings.mysql.password, database: settings.mysql.database, debug: settings.mysql.debug});

var loadSettings = function(cid,callback) {
    var settings = {
        defaultBackgroundImageEmpty: 'emptyRoom.png', //default image
        defaultBackgroundImageOccupied: 'noimg.png',  //default image
        defaultBackgroundColor: '555555',  //default background color (555555)
        fadeBackgroundImage: '0.25',  //fade background image (0.25)
                                    //0.0=no fade 
                                    //0.5 50% fade 
                                    //1.0 100% fade (white)
        textShadowsOffset: '4',       //shadows offset
        textShadowsFade: '5',         //shadows fade
        textShadowsColor: '000000',    //shadows color
        textColorDefault: 'ffffff',   //default text color
        occupiedText: 'Session in progress', //default room-in-use text
        emptyText: 'VACANT',
        slideshowInterval: '4500',    //duration for each slideshow in milliseconds
        refreshTimeout: '30*1000',    //frequency check
        eventListBackground: '222041',
        eventListTextColor: 'DBB072',
        magnifyFactorHeader: 2.35,
        magnifyFactorMain: 0.8,
        magnifyFactorTime: 1.35,
        magnifyFactorDesc: 2.35,
        magnifyFactorToday: 1.0,
        magnifyFactorTodayNoEvt: 1.5,
        upcomingEventText: 'Upcoming Events',
        noUpcomingEventText: 'No Upcoming Event'
    };
    if(cid!=-666) {
        pool.query("SELECT * FROM `calendarsettings` WHERE `calendarsettings`.`cid`="+cid, function(err,rows,fields) {
            if(err) return callback(err);
            if(rows.length!=0) {
                settings = {
                    defaultBackgroundImageEmpty: rows[0].defaultBackgroundImageEmpty,
                    defaultBackgroundImageOccupied: rows[0].defaultBackgroundImageOccupied,
                    defaultBackgroundColor: rows[0].defaultBackgroundColor,
                    fadeBackgroundImage: rows[0].fadeBackgroundImage,
                    textShadowsOffset: rows[0].textShadowsOffset,
                    textShadowsFade: rows[0].textShadowsFade,
                    textShadowsColor: rows[0].textShadowsColor,
                    textColorDefault: rows[0].textColorDefault,
                    occupiedText: rows[0].occupiedText,
                    emptyText: rows[0].emptyText,
                    slideshowInterval: rows[0].slideshowInterval,
                    refreshTimeout: rows[0].refreshTimeout,
                    eventListBackground: rows[0].eventListBackground,
                    eventListTextColor: rows[0].eventListTextColor,
                    magnifyFactorHeader: rows[0].magnifyFactorHeader,
                    magnifyFactorMain: rows[0].magnifyFactorMain,
                    magnifyFactorTime: rows[0].magnifyFactorTime,
                    magnifyFactorDesc: rows[0].magnifyFactorDesc,
                    magnifyFactorToday: rows[0].magnifyFactorToday,
                    magnifyFactorTodayNoEvt: rows[0].magnifyFactorTodayNoEvt,
                    upcomingEventText: rows[0].upcomingEventText,
                    noUpcomingEventText: rows[0].noUpcomingEventText
                };
            }
            callback(null, settings);
        });
    } else {
        callback(null, settings);
    }
}

module.exports = loadSettings;