// imports////////////////////////////////////////////////////////////
var express      = require('express');
var router       = express.Router();
//////////////////////////////////////////////////////////////////////

//controllers for each page///////////////////////////////////////////
var index = require('../controllers/index.controller');
var events = require('../controllers/events.json.controller');
var edit = require('../controllers/edit.controller');
var uploads = require('../controllers/uploads.controller');
var display = require('../controllers/display.controller');
var manage = require('../controllers/manage.controller');
var manage_settings = require('../controllers/manage.settings.controller');
var manage_global_settings = require('../controllers/manage.globalsettings.controller');
//////////////////////////////////////////////////////////////////////

//INDEX STUFF AND THINGS//////////////////////////////////////////////
router.get('/', index);
router.get('/events.json', events);
//////////////////////////////////////////////////////////////////////

//EDIT EVENT PAGES////////////////////////////////////////////////////
router.get('/edit', function(req,res) { res.redirect('/'); });
router.get('/edit/:uid', edit.get);
router.post('/edit/:uid', edit.post);
//////////////////////////////////////////////////////////////////////

//EDIT EVENT PAGE - UPLOAD IMAGE ELEMENTS/////////////////////////////
router.post("/uploads", uploads.post_main);
router.delete("/uploads/:uuid", uploads.delete_main);
router.get("/uploads/session", uploads.get_session);
//////////////////////////////////////////////////////////////////////

//DISPLAY SECTION - MAIN ZONE AND SUB ZONE////////////////////////////
router.get('/display', function(req,res) { res.redirect('/') });
router.get('/displaySvc/current/', function(req,res) { res.redirect('/') });
router.get('/displayToday/', function(req,res) { res.redirect('/') });
router.get('/displaySvc/today/', function(req,res) { res.redirect('/') });
router.get('/display/:cid', display.get_display);
router.get('/displaySvc/current/:cid', display.get_displaySvc_current);
router.get('/displayToday/:cid', display.get_displayToday);
router.get('/displaySvc/today/:cid', display.get_displaySvc_today);
router.get('/displayAll/', display.get_displayAllToday);
router.get('/displaySvc/all', display.get_displaySvc_allToday);
router.get('/displayAllUpcoming/', display.get_displayAllUpcomingToday);
router.get('/displaySvc/allUpcoming', display.get_displaySvc_allUpcomingToday);
//////////////////////////////////////////////////////////////////////

//CALENDAR MANAGEMENT SECTION/////////////////////////////////////////
router.get('/manage/delete', function(req,res) { res.redirect('/manage'); });
router.get('/manage/settings', function(req,res) { res.redirect('/manage'); });
router.post('/manage/settings', function(req,res) { res.sendStatus(405); });
router.get('/manage', manage.get_manage);
router.post('/manage', manage.post_manage);
router.get('/manage/delete/:cid', manage.get_manage_delete);
router.post('/manage/test', manage.post_manage_test);
router.get('/manage/settings/:cid', manage_settings.get);
router.post('/manage/settings/:cid', manage_settings.post);
router.get('/manage/globalsettings/', manage_global_settings.get);
router.post('/manage/globalsettings/', manage_global_settings.post);
//////////////////////////////////////////////////////////////////////

module.exports = router;
