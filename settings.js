var settings = {
	version: '0.1a',
	mysql: { connectionLimit: 100,
               host: 'localhost',
               user: 'parfaitcal',
               password: 'parfaitcal',
               database: 'calendar',
               debug: false
   }
};

module.exports = settings;