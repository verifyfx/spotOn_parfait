-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Aug 31, 2016 at 06:17 AM
-- Server version: 10.1.13-MariaDB
-- PHP Version: 5.6.23

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `calendar`
--
USE `parfait_calendar`;

-- --------------------------------------------------------

--
-- Table structure for table `calendars`
--

CREATE TABLE `calendars` (
  `cid` int(11) NOT NULL,
  `ical` varchar(256) NOT NULL,
  `description` varchar(64) DEFAULT NULL,
  `floor` varchar(64) NOT NULL,
  `hash` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `calendars`
--

INSERT INTO `calendars` (`cid`, `ical`, `description`, `floor`, `hash`) VALUES
(1, 'https://calendar.google.com/calendar/ical/e967vrnrope39cskkfaberhodk%40group.calendar.google.com/private-4858ee3e3fd6cea4f4f4043142cfe789/basic.ics', 'ห้อง No Noob', '55', 'ed26ad802e12cb869aa7f6464c55e521'),
(2, 'https://calendar.google.com/calendar/ical/mj21aaflgiqvegli14uv6m808c%40group.calendar.google.com/private-ae2cec3e4b82be5c4ec0e94560f6d975/basic.ics', 'ห้อง อภิมหาทัพ', '4', '0330a5110ced80098cce5ec398f6116d'),
(3, 'https://calendar.google.com/calendar/ical/ga92uodeombodsg4569390jdq0@group.calendar.google.com/private-848a2c4ee452c9f15fee92c435e9a7e2/basic.ics', 'Grand Ballroom IV', '2', '7bd562af23b6153c32518017865b4e57');

-- --------------------------------------------------------

--
-- Table structure for table `calendarsettings`
--

CREATE TABLE `calendarsettings` (
  `cid` int(11) NOT NULL COMMENT 'calendar id',
  `defaultBackgroundImageEmpty` varchar(250) NOT NULL DEFAULT 'emptyRoom.png' COMMENT 'background picture when no one is using',
  `defaultBackgroundImageOccupied` varchar(250) NOT NULL DEFAULT 'noimg.png' COMMENT 'background picture when the room is being used',
  `defaultBackgroundColor` varchar(6) NOT NULL DEFAULT '555555' COMMENT 'background color (in case of image 404) IN HEX',
  `fadeBackgroundImage` float NOT NULL DEFAULT '0.25' COMMENT 'background fade intensity (the higher, the darker)(0=0%opacity 0.5=50% opacity 1.0=100% opacity)',
  `textShadowsOffset` int(11) NOT NULL DEFAULT '4' COMMENT 'shadows offset of text',
  `textShadowsFade` int(11) NOT NULL DEFAULT '5' COMMENT 'shadows fade (0=sharp 10=blurry)',
  `textShadowsColor` varchar(6) NOT NULL DEFAULT '000000' COMMENT 'shadows color IN HEX',
  `textColorDefault` varchar(6) NOT NULL DEFAULT 'ffffff' COMMENT 'text color IN HEX',
  `occupiedText` varchar(128) NOT NULL DEFAULT 'Session in progress' COMMENT 'text to display on top of the screen when the room is being used',
  `emptyText` varchar(128) NOT NULL DEFAULT 'VACANT' COMMENT 'text to display when the room is not being used',
  `slideshowInterval` int(11) NOT NULL DEFAULT '4500' COMMENT 'duration for each picture in millis',
  `refreshTimeout` int(11) NOT NULL DEFAULT '30000' COMMENT 'how long will the calendar check for update',
  `eventListBackground` varchar(6) NOT NULL DEFAULT '222041' COMMENT 'event table background color IN HEX',
  `eventListTextColor` varchar(6) NOT NULL DEFAULT 'DBB072' COMMENT 'event table text color IN HEX',
  `magnifyFactorHeader` float NOT NULL DEFAULT '2.35',
  `magnifyFactorMain` float NOT NULL DEFAULT '0.8',
  `magnifyFactorTime` float NOT NULL DEFAULT '1.35',
  `magnifyFactorDesc` float NOT NULL DEFAULT '2.35',
  `magnifyFactorToday` float NOT NULL DEFAULT '1',
  `magnifyFactorTodayNoEvt` float NOT NULL DEFAULT '1.5',
  `upcomingEventText` varchar(64) NOT NULL DEFAULT 'Upcoming Events',
  `noUpcomingEventText` varchar(64) NOT NULL DEFAULT 'No Upcoming Event'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `calendarsettings`
--

INSERT INTO `calendarsettings` (`cid`, `defaultBackgroundImageEmpty`, `defaultBackgroundImageOccupied`, `defaultBackgroundColor`, `fadeBackgroundImage`, `textShadowsOffset`, `textShadowsFade`, `textShadowsColor`, `textColorDefault`, `occupiedText`, `emptyText`, `slideshowInterval`, `refreshTimeout`, `eventListBackground`, `eventListTextColor`, `magnifyFactorHeader`, `magnifyFactorMain`, `magnifyFactorTime`, `magnifyFactorDesc`, `magnifyFactorToday`, `magnifyFactorTodayNoEvt`, `upcomingEventText`, `noUpcomingEventText`) VALUES
(1, 'emptyRoom.png', 'noimg.png', '555555', 0.25, 4, 5, '000000', 'ffffff', 'Session in progress', 'VACANT', 4500, 30000, '222041', 'DBB072', 2.35, 0.8, 1.35, 2.35, 1, 1.5, 'Upcoming Events', 'No Upcoming Event'),
(2, 'emptyRoom.png', 'noimg.png', '555555', 0.25, 4, 5, '000000', 'ffffff', 'Session in progress', 'VACANT', 4500, 30000, '222041', 'dbb072', 2.35, 0.8, 1.35, 2.35, 1, 1.5, 'Upcoming Events', 'No Upcoming Event'),
(3, 'emptyRoom.png', 'noimg.png', '555555', 0.25, 4, 5, '000000', 'ffffff', 'Session in progress', 'VACANT', 4500, 30000, '222041', 'DBB072', 2.35, 0.8, 1.35, 2.35, 1, 1.5, 'Upcoming Events', 'No Upcoming Event');

-- --------------------------------------------------------

--
-- Table structure for table `eventproperties`
--

CREATE TABLE `eventproperties` (
  `uid` varchar(64) CHARACTER SET latin1 NOT NULL COMMENT 'UID generated by Google',
  `textColor` varchar(6) NOT NULL DEFAULT 'ffffff'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `eventproperties`
--

INSERT INTO `eventproperties` (`uid`, `textColor`) VALUES
('077ikf80r4tbbu7hce692bnpjg@google.com', 'ffffff'),
('0bqivc3hvcatstpv31kbf8det8@google.com', 'ffffff'),
('1ub3oq1v05mkemfku4e6v40fmk@google.com', 'ffffff'),
('29pkogrb58jct1ili5ugg64eag@google.com', 'ffffff'),
('2uvikj54u6ohj5lunudhf3b4tk@google.com', 'ffffff'),
('3noml18r4cjg12ah85q4farq58@google.com', 'ffffff'),
('3noml18r4cjg12ah85q4farq58@google.com_0', 'ffffff'),
('3noml18r4cjg12ah85q4farq58@google.com_1', 'ffffff'),
('480eofm8gbjdhmk1q83nhrkbko@google.com', 'ffffff'),
('497hlm1nmemjk17hq443dradv4@google.com', 'ffffff'),
('4dn92bapdvh7n2tt5fd90uavl8@google.com', 'ffffff'),
('4em4rqmb8secqdnvk04lbonr38@google.com', 'ffffff'),
('72a461lckqvdracfkipc34l91g@google.com', 'ffffff'),
('7enj4beif9a1ev658vohovcmi4@google.com', 'ffffff'),
('8qn2jjt7vpj5eup3dq4r141o1o@google.com', 'ffffff'),
('9d9pigf8a8nnpbmks8k5b84btk@google.com', 'ffffff'),
('9vlm13j3430ln429uboh34c3q8@google.com', 'ffffff'),
('ajlkq4qdj7e7jrakmrg17nr5rc@google.com', 'ffffff'),
('bf5tbpm6g3jee6hs0ntkpldh90@google.com', 'ffffff'),
('bhlqhke1738a1q74fjgs07lr3s@google.com', 'ffffff'),
('c0mduoj2ul0m9fpd4db4b00rvs@google.com', 'ffffff'),
('c2gqa60gqb00lkmcm8u4qh8g68@google.com', 'ffffff'),
('c2gqa60gqb00lkmcm8u4qh8g68@google.com_0', 'ffffff'),
('c2gqa60gqb00lkmcm8u4qh8g68@google.com_1', 'ffffff'),
('c48nfuuho51608d8cogotori5g@google.com', 'ffffff'),
('d1s8rt1sv4mdt071st66s3ckfc@google.com', 'ffffff'),
('hoer3e67veb9suh889lfv9f7mo@google.com', 'ffffff'),
('jc9vl0g692m2qa5s75n2tlajnk@google.com', 'ff0000'),
('lj0k1oojc6giervhs9kj5if4fk@google.com', 'ffffff'),
('nognucdrc3kfnrk8kd42be1b4g@google.com', 'ffffff'),
('nt7hhbb565skfu8ovmms8mqnbg@google.com', 'ffffff'),
('oqmfrcv1t3dp57ia8ecs0o2afc@google.com', 'ffffff'),
('p6slfimoeojk443u28h97emog4@google.com', 'ffffff'),
('plllof2f19mkuu9uo8majg1h40@google.com', 'ffffff'),
('pn340bcd97ud60aab8cqiro5io@google.com', 'ffffff'),
('punl3fmp6qn6vev2sgfji8vjnc@google.com', 'ffffff'),
('qc4bmuak6ts8p9b18lkceq6ero@google.com', 'ffffff'),
('srib37gnmubpaiimbdnqmonfak@google.com', 'ffffff'),
('u8sf6rgqi8ge8rg2r0qa8q2jgs@google.com', 'ffffff'),
('vk96kkig6388d78krprpqvvtbg@google.com', 'ffffff');

-- --------------------------------------------------------

--
-- Table structure for table `events`
--

CREATE TABLE `events` (
  `uid` varchar(64) CHARACTER SET latin1 NOT NULL COMMENT 'Google Generated UID',
  `cid` int(11) NOT NULL,
  `name` text,
  `startdate` varchar(13) NOT NULL DEFAULT '0',
  `enddate` varchar(13) NOT NULL DEFAULT '0',
  `description` text,
  `lastmodified` varchar(13) NOT NULL,
  `hash` varchar(40) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `events`
--

INSERT INTO `events` (`uid`, `cid`, `name`, `startdate`, `enddate`, `description`, `lastmodified`, `hash`) VALUES
('077ikf80r4tbbu7hce692bnpjg@google.com', 1, 'ประชุม Dev', '1469766600000', '1469770200000', 'ประชุม dev', '1469684864000', ''),
('0bqivc3hvcatstpv31kbf8det8@google.com', 2, 'กล่าวขอบคุณท่าน นายกรัฐมนตรี', '1472180400000', '1472182200000', 'แสดงความปลื้มปิติยินดีในการแสดงจุดมุงหมายในการแก้ปัญหาของท่านนายก', '1472204996000', ''),
('1ub3oq1v05mkemfku4e6v40fmk@google.com', 1, 'ประกาศกลุ่มการแข่งขันของเหล่าเกมส์เมอร์', '1472632200000', '1472641200000', 'แบ่งกลุ่มการแข่งขันเพื่อคัดเลือกผู้เข้ารอบ', '1472613856000', ''),
('29pkogrb58jct1ili5ugg64eag@google.com', 1, 'ประกาศรายชื่อผู้เข้ารอบต่อไป', '1472655600000', '1472659200000', 'ทำการแสดงรายชื่อผู้ชนะในแต่ระกลุ่ม', '1472613861000', ''),
('2uvikj54u6ohj5lunudhf3b4tk@google.com', 1, 'ppicccture', '1469791800000', '1469795400000', '', '1469780321000', ''),
('3noml18r4cjg12ah85q4farq58@google.com', 2, 'Helllo', '1472700600000', '1472704200000', '', '1472613862000', ''),
('3noml18r4cjg12ah85q4farq58@google.com_0', 2, 'Helllo', '1472268600000', '1472272200000', '', '1472528287000', ''),
('3noml18r4cjg12ah85q4farq58@google.com_1', 2, 'Helllo', '1472355000000', '1472358600000', '', '1472528287000', ''),
('480eofm8gbjdhmk1q83nhrkbko@google.com', 2, 'ประชุมเพื่อพูดคุยในหัวข้อแก้ปัญหารถติดใน กทม. อย่างยังยืน', '1472191200000', '1472202000000', 'พูดถึงปัญหาความหนาแน่นของการใช้รถใช้ถนนใน กทม. พร้อมหาวิธีแก้ไขปัญหา', '1472205201000', ''),
('497hlm1nmemjk17hq443dradv4@google.com', 3, 'example event', '1471588200000', '1471591800000', '', '1471578398000', ''),
('4dn92bapdvh7n2tt5fd90uavl8@google.com', 2, 'Dev meeting', '1471581000000', '1471586400000', 'ประชุมที่ spoton', '1471492470000', ''),
('4em4rqmb8secqdnvk04lbonr38@google.com', 2, 'Lunch Time', '1472623200000', '1472626800000', 'กินข้าว', '1472613857000', ''),
('72a461lckqvdracfkipc34l91g@google.com', 1, 'ให้ผู้เข้าแข่งขันร่วมกิจกรรมพร้อมมอบของรางวัล', '1472644800000', '1472655600000', 'ร่วมกิจกรรมเพื่อสร้างความผ่อนคลายให้แก้ผู้เข้าแข่งขัน', '1472613859000', ''),
('7enj4beif9a1ev658vohovcmi4@google.com', 1, 'test', '1471885200000', '1472230800000', '', '1471335508000', ''),
('8qn2jjt7vpj5eup3dq4r141o1o@google.com', 2, 'ประชุมการฝึกงานภาคฤดูร้อน', '1471921200000', '1471923000000', 'By Mingmanas Sivaraksa', '1471922859000', ''),
('9d9pigf8a8nnpbmks8k5b84btk@google.com', 1, 'ให้ผู้เข้าแข่งขันทำการเตรียมอุปกรณ์', '1472619600000', '1472623200000', 'เกมส์เมอร์ทำการติดตั้งอุปกรณ์ต่างๆเพื่อใช้ในการแข่งขัน', '1472613847000', ''),
('9vlm13j3430ln429uboh34c3q8@google.com', 2, 'work', '1472610600000', '1472621400000', '555', '1472613843000', ''),
('ajlkq4qdj7e7jrakmrg17nr5rc@google.com', 1, 'เริ่มการแข่งขัน', '1472641200000', '1472644800000', 'เริ่มการแข่งขันรอบคัดเลือก', '1472613858000', ''),
('bf5tbpm6g3jee6hs0ntkpldh90@google.com', 2, 'ร่วมจ็อกกิงเพื่อสุขภาพกับท่านนายกรัฐมนตรี', '1472212800000', '1472216400000', 'ร่วมรณรงค์ให้ประชนได้มองเห็นถึงการดูแลสุขภาพของตน พร้อมแสดงให้เห็นการแก้ปัยหารถติดอีกหนึ่งวิธีการคือ หากเดินทางไม่ไกลควรจะลดการใช้รถและถนน', '1472205337000', ''),
('bhlqhke1738a1q74fjgs07lr3s@google.com', 2, 'Time Event', '1471683600000', '1471696200000', 'เวลา เวลา เวลา\nบรรทัด 2', '1471603322000', ''),
('c0mduoj2ul0m9fpd4db4b00rvs@google.com', 2, 'ประชุม Dev2', '1472799600000', '1472803200000', 'This is description นะจ้ะ', '1472613851000', ''),
('c2gqa60gqb00lkmcm8u4qh8g68@google.com', 2, 'Repeat Event', '1471485600000', '1471494600000', 'Description Repeat Event', '1471492923000', ''),
('c2gqa60gqb00lkmcm8u4qh8g68@google.com_0', 2, 'Repeat Event', '1471485600000', '1471496400000', 'Description Repeat Event', '1471334423000', ''),
('c2gqa60gqb00lkmcm8u4qh8g68@google.com_1', 2, 'Repeat Event', '1471572000000', '1471582800000', 'Description Repeat Event', '1471334423000', ''),
('c48nfuuho51608d8cogotori5g@google.com', 1, 'พักรับประทานอาหารกลางวัน', '1472623200000', '1472628600000', 'ให้ผู้เข้าแข่งขันแยกย้างไปรับประมาณอาหาร', '1472613849000', ''),
('d1s8rt1sv4mdt071st66s3ckfc@google.com', 2, 'มอบของที่ระลึกแก่วิทยากร', '1472182200000', '1472184000000', 'มองสินน้ำใจเล้กน้อยแก้ผู้ที่มาร่วมองค์ประชุม', '1472205039000', ''),
('hoer3e67veb9suh889lfv9f7mo@google.com', 2, 'กล่าวต้อนรับท่านนายกรัฐมนตรี', '1472173200000', '1472176800000', 'กล่าวตอนรับเนื่องในโอกาศการประชุมนโยบายการจัดการปัญหาระดับชาติ', '1472204878000', ''),
('jc9vl0g692m2qa5s75n2tlajnk@google.com', 1, 'เจ้าหน้าที่ทำการตรวจสอบอุปกรณ์ของผู้เข้าแข่งขัน', '1472628600000', '1472632200000', 'ตรวจสอบหาอุปกรณ์หรือผู้เข้าแข่งขันที่ไม่เป็นไปตามกฎกติกา', '1472613853000', ''),
('lj0k1oojc6giervhs9kj5if4fk@google.com', 2, 'ทำอาหารกับท่านนายกรัฐมนตรี', '1472216400000', '1472220000000', 'ร่วมโครงการจัดทำอาหารเพื่อสุขภาพ โดยเหล่าคณะรัฐมนตรี', '1472205373000', ''),
('nognucdrc3kfnrk8kd42be1b4g@google.com', 2, 'ไป The Mall', '1472630400000', '1472637600000', '', '1472613855000', ''),
('nt7hhbb565skfu8ovmms8mqnbg@google.com', 2, 'ลองทดสอบใช้นโยบายแก้ปัญหารถติดที่ได้จากการประชุม', '1472202000000', '1472212800000', 'ท่านนายกรัฐมนตรีและคณะร่วมกันทดสอบแผนการแก้ไขปัญหารถติด', '1472205248000', ''),
('oqmfrcv1t3dp57ia8ecs0o2afc@google.com', 2, 'เดินทางรับประธานอาหารกลางวัน', '1472184000000', '1472191200000', 'รับประทานอาหารจากกลุ่มแม่บ้านของ กทม. ที่ร่วมกันจัดทำมาเพื่อต้อนรับผู้ที่มาร่วมประชุม', '1472205146000', ''),
('p6slfimoeojk443u28h97emog4@google.com', 1, 'รวมเหล่าเกมเมอร์', '1472610600000', '1472614200000', 'นัดรวมตัวเหล่าเกมส์เมอร์จากทั่วประเทศหน้าห้องประชุม', '1472613845000', ''),
('plllof2f19mkuu9uo8majg1h40@google.com', 2, 'All Day Event', '1471712400000', '1471798799999', 'Description All Day Event', '1471839790000', ''),
('pn340bcd97ud60aab8cqiro5io@google.com', 2, 'รับฟังการกล่าวให้โอวาท จาก นายกรัฐมนตรี', '1472176800000', '1472180400000', 'ท่านนายกรัฐมนตรีกล่าวอธิบายจุดมุ้งหมายของการประชุมในครั้งนี้', '1472204926000', ''),
('punl3fmp6qn6vev2sgfji8vjnc@google.com', 2, 'brand new event', '1472034600000', '1472040000000', '', '1472092446000', ''),
('qc4bmuak6ts8p9b18lkceq6ero@google.com', 1, 'ร่วมฟังคำชี้แจงในการแข่งขัน', '1472614200000', '1472619600000', 'รับฟังกฎและข้อบังคับต่างๆในการแข่งขัน', '1472613846000', ''),
('srib37gnmubpaiimbdnqmonfak@google.com', 1, 'Today', '1471366800000', '1471453199999', '', '1471407832000', ''),
('u8sf6rgqi8ge8rg2r0qa8q2jgs@google.com', 1, 'test', '1469863800000', '1469867400000', 'test', '1469779741000', ''),
('vk96kkig6388d78krprpqvvtbg@google.com', 2, 'DayLong Event ', '1471588200000', '1471633200000', 'บรรทัด 1\nบรรทัด 2\nบรรทัด 3\nบรรทัด 4\nบรรทัด 5', '1471599342000', '');

-- --------------------------------------------------------

--
-- Table structure for table `globalsettings`
--

CREATE TABLE `globalsettings` (
  `xid` int(11) NOT NULL,
  `allevent_textColor` varchar(6) NOT NULL,
  `allevent_backgroundColor` varchar(6) NOT NULL,
  `allevent_headlineText` varchar(128) NOT NULL,
  `allevent_headlineTextNoEvent` varchar(128) NOT NULL,
  `allevent_magnifyFactorHead` float NOT NULL,
  `allevent_magnifyFactorTableHead` float NOT NULL,
  `allevent_magnifyFactorContent` float NOT NULL,
  `allevent_refreshTimeout` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `globalsettings`
--

INSERT INTO `globalsettings` (`xid`, `allevent_textColor`, `allevent_backgroundColor`, `allevent_headlineText`, `allevent_headlineTextNoEvent`, `allevent_magnifyFactorHead`, `allevent_magnifyFactorTableHead`, `allevent_magnifyFactorContent`, `allevent_refreshTimeout`) VALUES
(1, 'dbb072', '222041', 'Today''s Events', 'No Upcoming Event', 1.5, 3, 3.3, 30000);

-- --------------------------------------------------------

--
-- Table structure for table `images`
--

CREATE TABLE `images` (
  `imgUUID` varchar(36) NOT NULL,
  `uid` varchar(64) CHARACTER SET utf8 NOT NULL,
  `imgName` varchar(192) CHARACTER SET utf8 NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `images`
--

INSERT INTO `images` (`imgUUID`, `uid`, `imgName`) VALUES
('11607b9c-04a9-4063-b35d-897881fbc955', '7enj4beif9a1ev658vohovcmi4@google.com', '18.jpg'),
('1529d9cc-daad-40d9-a632-3a928ce1a00b', 'c48nfuuho51608d8cogotori5g@google.com', 'o8lumsk3hhE7rJ2Xl6C-o.jpg'),
('19058ede-b7e4-4406-8c17-bcc92bf72ce7', 'nt7hhbb565skfu8ovmms8mqnbg@google.com', '12604805_10153865502740699_2759601530136060434_o.jpg'),
('1d7d617e-c6a9-480a-bd0e-56831c8b36c5', 'p6slfimoeojk443u28h97emog4@google.com', '4k-image-tiger-jumping.jpg'),
('1df68d19-260b-4133-aabb-75d77ea360b1', '0bqivc3hvcatstpv31kbf8det8@google.com', 'a1400-kunrai.jpg'),
('1fbf7df3-76a1-48dd-960f-9318ccc63827', '1ub3oq1v05mkemfku4e6v40fmk@google.com', '0IvbPid.png'),
('23ab74b3-6a42-4372-b6ec-2c4293c950aa', 'tsm2pebrvbtd56r7i7ekllcvdk@google.com', 'EMQUARTIER_ZAICHI.png'),
('240c11c0-cada-43c9-9529-c4f3e0b218e7', 'd1s8rt1sv4mdt071st66s3ckfc@google.com', '12642567_10153880605745699_1421261456293666679_n.jpg'),
('24fc068b-1c93-45e8-b4cf-1189e033c18e', 'plllof2f19mkuu9uo8majg1h40@google.com', '4.jpg'),
('25893a79-0fe6-4c03-b45d-e95554846fb6', '9d9pigf8a8nnpbmks8k5b84btk@google.com', 'maxresdefault.jpg'),
('29d58281-d6f0-4086-8031-a59f79d68297', 'bf5tbpm6g3jee6hs0ntkpldh90@google.com', 'a.png'),
('2a320470-c4c6-48b4-abdb-39571bb7a4f1', '29pkogrb58jct1ili5ugg64eag@google.com', '1430628263-1913433829-o.jpg'),
('2cb1d1b9-e6bf-4233-b228-daaec148362e', '4em4rqmb8secqdnvk04lbonr38@google.com', '5.jpg'),
('3300aafe-45f9-40d5-afc9-7a996f122a84', 'plllof2f19mkuu9uo8majg1h40@google.com', '1.jpg'),
('334ddb3a-4463-41ac-a5cd-b01e3402104b', 'tsm2pebrvbtd56r7i7ekllcvdk@google.com', '12647280_10153874232575699_463961162717073917_n.jpg'),
('3720a0eb-e4fe-4ac4-a656-5185dbfa70c5', 'tsm2pebrvbtd56r7i7ekllcvdk@google.com', '1181983109892718390_23752734_standard_resolution.jpg'),
('41c26b36-d974-402e-8529-63172b9142e1', 'tsm2pebrvbtd56r7i7ekllcvdk@google.com', 'EMQUARTIER_Starbucks.png'),
('4337f592-0e7e-42ab-9cd8-20a89a7f06d6', 'tsm2pebrvbtd56r7i7ekllcvdk@google.com', 'EMQUARTIER_BURGER KING..png'),
('47ecf79b-8774-4163-b025-f0e6c63337ad', 'c0mduoj2ul0m9fpd4db4b00rvs@google.com', '7.jpg'),
('4ceccbe9-92b3-426c-bd5b-b3c927c986e1', 'p6slfimoeojk443u28h97emog4@google.com', '05-ภาพกิจกรรม-Nissan-GT-Academy.jpg'),
('4d63ddfe-332f-41fc-b823-12f121158235', 'tsm2pebrvbtd56r7i7ekllcvdk@google.com', '12615580_950397661693487_9171625146159915467_o.jpg'),
('5056ac8b-12a7-4fd8-98e6-1dfeff60291c', 'tsm2pebrvbtd56r7i7ekllcvdk@google.com', '12646766_10153865504015699_6464454160057817541_o.jpg'),
('5413639e-efb8-4053-9a7b-62f655ab22b2', '4em4rqmb8secqdnvk04lbonr38@google.com', '11.jpg'),
('55ae7470-0774-45f1-9e44-2fb5a25a13f6', 'tsm2pebrvbtd56r7i7ekllcvdk@google.com', '12605412_10153865502820699_6046240483007190035_o.jpg'),
('5649c501-6ca6-45aa-a46d-ef0e93560070', 'bf5tbpm6g3jee6hs0ntkpldh90@google.com', 'b.jpg'),
('572ecab2-66e2-4c8c-b53d-bb57eed116c4', 'tsm2pebrvbtd56r7i7ekllcvdk@google.com', '12719382_10153892050115699_7100429176068122695_o.jpg'),
('626e12ee-726c-497a-958a-f627da5a70da', 'jc9vl0g692m2qa5s75n2tlajnk@google.com', '1412964119043.jpg'),
('627e6cb6-7109-4e55-b949-c839a09f3725', 'nognucdrc3kfnrk8kd42be1b4g@google.com', '17.jpg'),
('63aad711-ffb2-4019-a209-9d99d06f31d6', '4em4rqmb8secqdnvk04lbonr38@google.com', '18.jpg'),
('647424da-da25-4831-a8aa-e47f9258cb13', '8qn2jjt7vpj5eup3dq4r141o1o@google.com', 'home.jpg'),
('676c3ea5-f599-4c03-9774-bf341dd6eb64', 'plllof2f19mkuu9uo8majg1h40@google.com', '5.jpg'),
('6f30d229-3f93-474f-bb46-3cdcdbffd6ce', 'p6slfimoeojk443u28h97emog4@google.com', '4k-image-tiger-jumping.jpg'),
('6fb30bb2-d7a4-4e36-95d0-0491d031dc39', 'tsm2pebrvbtd56r7i7ekllcvdk@google.com', 'EMQUARTIER_BONCHON.png'),
('6ff13bdc-a645-4f6b-a7eb-ba7cd7d0d8ec', 'd1s8rt1sv4mdt071st66s3ckfc@google.com', '12419383_10153813217830699_6707649742374329052_o.jpg'),
('6ffe5a37-1364-4e1c-bad0-d82f4d492540', 'nt7hhbb565skfu8ovmms8mqnbg@google.com', '12631350_950418308358089_83322642457541122_n.jpg'),
('77019cc8-9438-42f4-8e6d-3dc20fc0b965', '0bqivc3hvcatstpv31kbf8det8@google.com', 'imgK.png'),
('7dba5f73-f338-47de-a96c-78a32c289acb', 'pn340bcd97ud60aab8cqiro5io@google.com', 'tiffy.jpg'),
('85b2792c-d861-47cd-9d53-d4ef79f078a7', 'tsm2pebrvbtd56r7i7ekllcvdk@google.com', '12642567_10153880605745699_1421261456293666679_n.jpg'),
('861137b8-69d6-4820-a8c6-6cfda21fefd1', 'tsm2pebrvbtd56r7i7ekllcvdk@google.com', 'EMQUARTIER_Nantsuttei.png'),
('8be93643-f68d-4619-8cb4-2aee686fa669', '8qn2jjt7vpj5eup3dq4r141o1o@google.com', '107282607.jpg'),
('8dad1575-e9eb-44bc-8929-8d15c8cd9dbe', 'tsm2pebrvbtd56r7i7ekllcvdk@google.com', '12631350_950418308358089_83322642457541122_n.jpg'),
('8de83fe2-f5dc-44bd-84a9-362b3cb3e728', 'tsm2pebrvbtd56r7i7ekllcvdk@google.com', 'EMQUARTIER_MAN KITCHEN BY CHEF MAN.png'),
('8f76fa06-1d3e-4eba-9268-0ba3a4f71674', 'bhlqhke1738a1q74fjgs07lr3s@google.com', '18.jpg'),
('96b73ede-dac4-4e05-90db-bc948e7dccb0', 'tsm2pebrvbtd56r7i7ekllcvdk@google.com', 'EMQUARTIER_THE NINTH CAFE.png'),
('974dbef0-badb-4404-83fd-f0c2956b361e', 'd1s8rt1sv4mdt071st66s3ckfc@google.com', '12719382_10153892050115699_7100429176068122695_o.jpg'),
('9bfa4d95-694c-4a00-9c1c-fe6f1e9b6f5e', '9vlm13j3430ln429uboh34c3q8@google.com', '12419383_10153813217830699_6707649742374329052_o.jpg'),
('a5273f0e-b361-41ba-a08d-441b6d3025f5', 'p6slfimoeojk443u28h97emog4@google.com', '4k-image-tiger-jumping.jpg'),
('a920a05b-60ba-4a85-ac18-301328772a5c', 'tsm2pebrvbtd56r7i7ekllcvdk@google.com', '12615648_10153859115930699_2139250007084497892_o.jpg'),
('ad9fe440-9d56-409e-98da-1d745e74e664', 'plllof2f19mkuu9uo8majg1h40@google.com', '3.jpg'),
('ae0963da-db9d-4d52-8def-fef2c940fb79', '4em4rqmb8secqdnvk04lbonr38@google.com', '4.jpg'),
('aef5d146-0368-4966-a3b1-a2e3cc805f84', 'nognucdrc3kfnrk8kd42be1b4g@google.com', '15.jpg'),
('af5dbcb2-b4c6-45e4-8ace-8135d357e7e3', 'ajlkq4qdj7e7jrakmrg17nr5rc@google.com', '86031_DSC00965.jpg'),
('b5962abf-ed4f-469f-84c1-f2b4de32fb52', '9vlm13j3430ln429uboh34c3q8@google.com', '12719382_10153892050115699_7100429176068122695_o.jpg'),
('b92078ce-897f-49dc-abcf-4d243b5857d1', 'bf5tbpm6g3jee6hs0ntkpldh90@google.com', 'd.jpg'),
('b961a6b6-9af5-4a91-9d0b-9371ff5ebb31', 'tsm2pebrvbtd56r7i7ekllcvdk@google.com', '17.jpg'),
('bc94c18d-ae4b-43f9-adbe-26f962f8ba7a', '4em4rqmb8secqdnvk04lbonr38@google.com', '19.jpg'),
('bd687b56-6127-43c8-9c78-0cda4ea472e3', '4em4rqmb8secqdnvk04lbonr38@google.com', '12.jpg'),
('be27d9db-ad2f-4180-bfb1-fb343deef44d', '480eofm8gbjdhmk1q83nhrkbko@google.com', '66897_full.jpg'),
('c7471308-4be5-45ab-9102-4ce1879d514b', 'nt7hhbb565skfu8ovmms8mqnbg@google.com', '12633621_10153865503950699_7550945723202879596_o.jpg'),
('d4cb1952-b2ce-477e-845c-3973e069f6c7', 'tsm2pebrvbtd56r7i7ekllcvdk@google.com', 'EMPORIUM_THE RAW BAR.png'),
('d523030a-d498-46fb-8afc-51715cabb34f', 'tsm2pebrvbtd56r7i7ekllcvdk@google.com', 'EMQUARTIER_NARA.png'),
('d63d3ae9-f163-4432-b410-8ac2157f1d60', 'bhlqhke1738a1q74fjgs07lr3s@google.com', '19.jpg'),
('d6c7371b-aa88-4580-8a5b-9a296761fed5', 'tsm2pebrvbtd56r7i7ekllcvdk@google.com', '12628470_10153865502795699_3155759569337098762_o.jpg'),
('d8c7eecc-2d57-44d2-88ec-f13a52f58039', '72a461lckqvdracfkipc34l91g@google.com', 'playstation-tcc2016-1.jpg'),
('da0dda65-925d-40d6-b518-e0aca0cb497b', '', '8.jpg'),
('dfb5f745-edcb-4d25-856c-f00fc6a03417', 'bf5tbpm6g3jee6hs0ntkpldh90@google.com', 'c.jpg'),
('e1ec2b18-05fc-4765-9453-1fa54793ecc9', 'plllof2f19mkuu9uo8majg1h40@google.com', '2.jpg'),
('e6160a18-5ac5-4eb9-9439-98ea2d83b305', '480eofm8gbjdhmk1q83nhrkbko@google.com', '255.jpg'),
('e65cf7d5-9651-4432-aec7-f9cd281847fd', '7enj4beif9a1ev658vohovcmi4@google.com', '9.jpg'),
('e6744074-c322-4cf0-b662-2978da13f690', 'tsm2pebrvbtd56r7i7ekllcvdk@google.com', '12633621_10153865503950699_7550945723202879596_o.jpg'),
('e6eedde8-0bed-4421-8b1f-e70d47103a5c', 'nognucdrc3kfnrk8kd42be1b4g@google.com', '14.jpg'),
('ebb02e3c-458f-46b6-90e5-4873887ec784', 'qc4bmuak6ts8p9b18lkceq6ero@google.com', '201512221420034-20061002145931-e1463403754832.jpg'),
('f6bc1e5a-7dac-4d6f-8d66-b911ab962f96', 'nt7hhbb565skfu8ovmms8mqnbg@google.com', '12628470_10153865502795699_3155759569337098762_o.jpg'),
('f6dd9c49-503c-4c6a-814c-a7aa1d293155', 'pn340bcd97ud60aab8cqiro5io@google.com', 'imgI.png'),
('fd0536a5-5fb5-4da2-8765-c36e2bdb452c', 'tsm2pebrvbtd56r7i7ekllcvdk@google.com', '12716094_10153892049980699_7939487719660555720_o.jpg'),
('fe79c79b-a95d-41fc-853c-dbd5f054d3b8', '7enj4beif9a1ev658vohovcmi4@google.com', '9.jpg');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `calendars`
--
ALTER TABLE `calendars`
  ADD PRIMARY KEY (`cid`);

--
-- Indexes for table `calendarsettings`
--
ALTER TABLE `calendarsettings`
  ADD PRIMARY KEY (`cid`);

--
-- Indexes for table `eventproperties`
--
ALTER TABLE `eventproperties`
  ADD PRIMARY KEY (`uid`);

--
-- Indexes for table `events`
--
ALTER TABLE `events`
  ADD PRIMARY KEY (`uid`);

--
-- Indexes for table `globalsettings`
--
ALTER TABLE `globalsettings`
  ADD PRIMARY KEY (`xid`);

--
-- Indexes for table `images`
--
ALTER TABLE `images`
  ADD PRIMARY KEY (`imgUUID`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `calendars`
--
ALTER TABLE `calendars`
  MODIFY `cid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `globalsettings`
--
ALTER TABLE `globalsettings`
  MODIFY `xid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
